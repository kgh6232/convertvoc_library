//
//  iData.h
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iData.h
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#pragma once

#include "iType.h"

/**
 * @class    iData
 * @brief    
 */

#define DEFLEN_ALLOC			(4096)

class iData
{
public:
	iData(iSize_t nUnit = DEFLEN_ALLOC);
	iData(const iData& rData);

	iData(NPCSTR szStr);
	iData(const iByte_t* pBytes, iSize_t nBytes);

	virtual ~iData();

	inline iSize_t Size() const { return mnData; };
	inline NPSTR Data() { return (NPSTR)mvData; };

	void SetData(const iData& rData);
	void SetString(NPCSTR szStr);
	void SetBytes(const iByte_t* pBytes, iSize_t nBytes);

	void AddString(NPCSTR szAdd);
	void AddBytes(const iByte_t* pBytes, iSize_t nBytes);
	void AddByte(const iByte_t cBytes);

	inline NPCSTR String() const 			{ return (NPCSTR)mvData; };
	inline const iByte_t* Bytes() const 	{ return mvData; };

	inline operator NPCSTR() const 			{ return String(); };
	inline operator const iByte_t*() const 	{ return Bytes(); };

	inline operator NPSTR() 				{ return (NPSTR)mvData; };
	inline operator iByte_t*() 				{ return mvData; };

	iData& operator=(const iData& rData);
	iData& operator=(NPCSTR szStr);

	void Clear();

private:
	iSize_t mnUnit;
	iSize_t mnAloc;
	iSize_t mnData;
	iByte_t* mvData;
};





































