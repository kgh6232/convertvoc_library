//
//  iG711.cpp
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iG711.cpp
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#include "iType.h"
#include "iG711.h"

/*
 * This source code is a product of Sun Microsystems, Inc. and is provided
 * for unrestricted use.  Users may copy or modify this source code without
 * charge.
 *
 * SUN SOURCE CODE IS PROVIDED AS IS WITH NO WARRANTIES OF ANY KIND INCLUDING
 * THE WARRANTIES OF DESIGN, MERCHANTIBILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE, OR ARISING FROM A COURSE OF DEALING, USAGE OR TRADE PRACTICE.
 *
 * Sun source code is provided with no support and without any obligation on
 * the part of Sun Microsystems, Inc. to assist in its use, correction,
 * modification or enhancement.
 *
 * SUN MICROSYSTEMS, INC. SHALL HAVE NO LIABILITY WITH RESPECT TO THE
 * INFRINGEMENT OF COPYRIGHTS, TRADE SECRETS OR ANY PATENTS BY THIS SOFTWARE
 * OR ANY PART THEREOF.
 *
 * In no event will Sun Microsystems, Inc. be liable for any lost revenue
 * or profits or other special, indirect and consequential damages, even if
 * Sun has been advised of the possibility of such damages.
 *
 * Sun Microsystems, Inc.
 * 2550 Garcia Avenue
 * Mountain View, California  94043
 */

/*
 * g711.c
 *
 * u-law, A-law and linear PCM conversions.
 */

/*
 * December 30, 1994:
 * Functions linear2alaw, linear2ulaw have been updated to correctly
 * convert unquantized 16 bit values.
 * Tables for direct u- to A-law and A- to u-law conversions have been
 * corrected.
 * Borge Lindberg, Center for PersonKommunikation, Aalborg University.
 * bli@cpk.auc.dk
 *
 */

#define	SIGN_BIT	(0x80)		/* Sign bit for a A-law byte. */
#define	QUANT_MASK	(0xf)		/* Quantization field mask. */
#define	NSEGS		(8)			/* Number of A-law segments. */
#define	SEG_SHIFT	(4)			/* Left shift for segment number. */
#define	SEG_MASK	(0x70)		/* Segment field mask. */

static int16 theSegAend[8] = {0x1F, 0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF};
static int16 theSegUend[8] = {0x3F, 0x7F, 0xFF, 0x1FF, 0x3FF, 0x7FF, 0xFFF, 0x1FFF};

/* copy from CCITT G.711 specifications */
/* u- to A-law conversions */
uint8 theCvtU2A[128] = {
	1,	1,	2,	2,	3,	3,	4,	4,
	5,	5,	6,	6,	7,	7,	8,	8,
	9,	10,	11,	12,	13,	14,	15,	16,
	17,	18,	19,	20,	21,	22,	23,	24,
	25,	27,	29,	31,	33,	34,	35,	36,
	37,	38,	39,	40,	41,	42,	43,	44,
	46,	48,	49,	50,	51,	52,	53,	54,
	55,	56,	57,	58,	59,	60,	61,	62,
	64,	65,	66,	67,	68,	69,	70,	71,
	72,	73,	74,	75,	76,	77,	78,	79,
	80,	82,	83,	84,	85,	86,	87,	88,
	89,	90,	91,	92,	93,	94,	95,	96,
	97,	98,	99,	100,101,102,103,104,
	105,106,107,108,109,110,111,112,
	113,114,115,116,117,118,119,120,
	121,122,123,124,125,126,127,128};
/* A- to u-law conversions */
uint8 theCvtA2U[128] = {
	1,	3,	5,	7,	9,	11,	13,	15,
	16,	17,	18,	19,	20,	21,	22,	23,
	24,	25,	26,	27,	28,	29,	30,	31,
	32,	32,	33,	33,	34,	34,	35,	35,
	36,	37,	38,	39,	40,	41,	42,	43,
	44,	45,	46,	47,	48,	48,	49,	49,
	50,	51,	52,	53,	54,	55,	56,	57,
	58,	59,	60,	61,	62,	63,	64,	64,
	65,	66,	67,	68,	69,	70,	71,	72,
	73,	74,	75,	76,	77,	78,	79,	80,
	80,	81,	82,	83,	84,	85,	86,	87,
	88,	89,	90,	91,	92,	93,	94,	95,
	96,	97,	98,	99,	100,101,102,103,
	104,105,106,107,108,109,110,111,
	112,113,114,115,116,117,118,119,
	120,121,122,123,124,125,126,127};

static int32 search(int val, short* table, int size)
{
	for(int32 i=0; i<size; i++)
	{
		if(val <= *table++) return (i);
	}
	return (size);
}








/*
 * linear2alaw() - Convert a 16-bit linear PCM value to 8-bit A-law
 *
 * linear2alaw() accepts an 16-bit integer and encodes it as A-law data.
 *
 *		Linear Input Code	Compressed Code
 *	------------------------	---------------
 *	0000000wxyza			000wxyz
 *	0000001wxyza			001wxyz
 *	000001wxyzab			010wxyz
 *	00001wxyzabc			011wxyz
 *	0001wxyzabcd			100wxyz
 *	001wxyzabcde			101wxyz
 *	01wxyzabcdef			110wxyz
 *	1wxyzabcdefg			111wxyz
 *
 * For further information see John C. Bellamy's Digital Telephony, 1982,
 * John Wiley & Sons, pps 98-111 and 472-476.
 */

uint8 Linear2aLaw(int16 vPcm)
{
	int32 nMask;
	int32 nSeg;
	uint8 aLaw;

	vPcm = vPcm >> 3;

	if (vPcm >= 0) {
		nMask = 0xD5;		/* sign (7th) bit = 1 */
	} else {
		nMask = 0x55;		/* sign bit = 0 */
		vPcm = -vPcm - 1;
	}

	/* Convert the scaled magnitude to segment number. */
	nSeg = search(vPcm, theSegAend, 8);

	/* Combine the sign, segment, and quantization bits. */

	if (nSeg >= 8)		/* out of range, return maximum value. */
		return (uint8) (0x7F ^ nMask);
	else {
		aLaw = (uint8) nSeg << SEG_SHIFT;
		if (nSeg < 2)
			aLaw |= (vPcm >> 1) & QUANT_MASK;
		else
			aLaw |= (vPcm >> nSeg) & QUANT_MASK;
		return (aLaw ^ nMask);
	}
}

int16 aLaw2Linear(uint8 aLaw)
{
	int32 nTemp;
	int32 nSeg;

	aLaw ^= 0x55;

	nTemp = (aLaw & QUANT_MASK) << 4;
	nSeg = ((unsigned)aLaw & SEG_MASK) >> SEG_SHIFT;
	switch (nSeg) {
	case 0:
		nTemp += 8;
		break;
	case 1:
		nTemp += 0x108;
		break;
	default:
		nTemp += 0x108;
		nTemp <<= nSeg - 1;
	}
	return ((aLaw & SIGN_BIT) ? nTemp : -nTemp);
}










#define	BIAS		(0x84)		/* Bias for linear code. */
#define CLIP		8159

/*
 * linear2ulaw() - Convert a linear PCM value to u-law
 *
 * In order to simplify the encoding process, the original linear magnitude
 * is biased by adding 33 which shifts the encoding range from (0 - 8158) to
 * (33 - 8191). The result can be seen in the following encoding table:
 *
 *	Biased Linear Input Code	Compressed Code
 *	------------------------	---------------
 *	00000001wxyza			000wxyz
 *	0000001wxyzab			001wxyz
 *	000001wxyzabc			010wxyz
 *	00001wxyzabcd			011wxyz
 *	0001wxyzabcde			100wxyz
 *	001wxyzabcdef			101wxyz
 *	01wxyzabcdefg			110wxyz
 *	1wxyzabcdefgh			111wxyz
 *
 * Each biased linear code has a leading 1 which identifies the segment
 * number. The value of the segment number is equal to 7 minus the number
 * of leading 0's. The quantization interval is directly available as the
 * four bits wxyz.  * The trailing bits (a - h) are ignored.
 *
 * Ordinarily the complement of the resulting code word is used for
 * transmission, and so the code word is complemented before it is returned.
 *
 * For further information see John C. Bellamy's Digital Telephony, 1982,
 * John Wiley & Sons, pps 98-111 and 472-476.
 */

uint8 Linear2uLaw(int16 vPcm)
{
	int32 nMask;
	int32 nSeg;
	uint8 uLaw;

	/* Get the sign and the magnitude of the value. */
	vPcm = vPcm >> 2;
	if (vPcm < 0) {
		vPcm = -vPcm;
		nMask = 0x7F;
	} else {
		nMask = 0xFF;
	}
	if ( vPcm > CLIP ) vPcm = CLIP;		/* clip the magnitude */
	vPcm += (BIAS >> 2);

	/* Convert the scaled magnitude to segment number. */
	nSeg = search(vPcm, theSegUend, 8);

	/*
	 * Combine the sign, segment, quantization bits;
	 * and complement the code word.
	 */
	if (nSeg >= 8)		/* out of range, return maximum value. */
		return (uint8) (0x7F ^ nMask);
	else {
		uLaw = (uint8) (nSeg << 4) | ((vPcm >> (nSeg + 1)) & 0xF);
		return (uLaw ^ nMask);
	}
}

int16 uLaw2Linear(uint8 uLaw)
{
	int32 nTemp;

	/* Complement to obtain normal u-law value. */
	uLaw = ~uLaw;

	/*
	 * Extract and bias the quantization bits. Then
	 * shift up by the segment number and subtract out the bias.
	 */
	nTemp = ((uLaw & QUANT_MASK) << 3) + BIAS;
	nTemp <<= ((unsigned)uLaw & SEG_MASK) >> SEG_SHIFT;

	return ((uLaw & SIGN_BIT) ? (BIAS - nTemp) : (nTemp - BIAS));
}

uint8 aLaw2uLaw(uint8 aLaw)
{
	aLaw &= 0xff;
	return ((aLaw & 0x80) ? (0xFF ^ theCvtA2U[aLaw ^ 0xD5]) :
	    (0x7F ^ theCvtA2U[aLaw ^ 0x55]));
}

uint8 uLaw2aLaw(uint8 uLaw)
{
	uLaw &= 0xff;
	return ((uLaw & 0x80) ? (0xD5 ^ (theCvtU2A[0xFF ^ uLaw] - 1)) :
	    (0x55 ^ (theCvtU2A[0x7F ^ uLaw] - 1)));
}


































