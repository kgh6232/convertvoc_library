//
//  iObj.h
//  iCore
//
//  Created by 류관중 on 12. 5. 14.
//  Copyright (c) 2012. Bridgetec. All rights reserved.
//

/**
 * @file    iObj.h
 * @brief   iCore Library의 최상위 Class
 *
 * @version v0.1
 * @see     없음
 * @date    2012. 5. 14.
 */

#pragma once
#include "iType.h"

#define ISETNAME		setObjName(__FUNCTION__)
#define IOBJPTR(t)		((t*)Object())
#define IOBJECT(t)		(*IOBJPTR(t))

/**
 * @class    iObj
 * @brief    iCore Library의 최상위 Class
 * @author   류관중(tony@bridgetec.co.kr)
 * @date     2012. 5. 14.
 */
class ILIBAPI iObj
{
public:
	iObj();
	virtual ~iObj();

	static iSize_t ObjCnt();
	NPCSTR ObjName();
	void setObjName(NPCSTR szFmt, ...);

protected:
	inline NPVOID& Object() { return mpObject; };
	inline const NPVOID Object() const { return mpObject; };

private:
	char mszObjName[ILEN_16B];
	NPVOID mpObject;
};

