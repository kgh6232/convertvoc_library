//
//  iG711.h
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iG711.h
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#pragma once

#include "iType.h"

uint8 Linear2aLaw(int16 vPcm);
int16 aLaw2Linear(uint8 aLaw);

uint8 Linear2uLaw(int16 vPcm);
int16 uLaw2Linear(uint8 uLaw);

uint8 aLaw2uLaw(uint8 aLaw);
uint8 uLaw2aLaw(uint8 uLaw);



































