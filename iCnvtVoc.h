
#pragma once

#include "iType.h"
#include "Common.h"

class iCnvtVoc {
public :
    iCnvtVoc();
    virtual ~iCnvtVoc();

    void Init(NPCSTR szOutDir);
    void Clear();
    int32 ConvertVoc(NPCSTR szVocFile, uint16 nAudioType);

    NPCSTR GetOutFilePath() { return mszOutFile; }
    uint64 GetOutFileSize() { return mnOutFileSize; }

protected :
    int32 Voc2Pcm(NPCSTR szIn, NPCSTR szOut);
    int32 Voc2MP3(NPCSTR szIn, NPCSTR szPCM, NPCSTR szOut);
    int32 Voc2Alaw(NPCSTR szIn, NPCSTR szOut);
    int32 DecodeVoc(NPCSTR szIn, NPCSTR szOut);

private :
    iChar_t mszOutDir[MAXLEN_LINE4];
    iChar_t mszOutFile[MAXLEN_LINE4];
    uint64 mnOutFileSize;

};