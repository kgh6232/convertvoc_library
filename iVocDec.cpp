//
//  iVocDec.cpp
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iVocDec.cpp
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#include "iG711.h"
#include "iVocDec.h"
#include "Common.h"

static int32 GetFormatFromSampleFmt(NPCSTR *ppFmt, enum AVSampleFormat eSampleFmt);
static void DispSampleFmt(const AVCodec *pCodec);
static void DispSampleRate(const AVCodec* pCodec);
static void DispChannelLayout(const AVCodec* pCodec);

iVocDec::iVocDec()
{
	Clear();
}

iVocDec::~iVocDec()
{
	FreeDecoder();
}

bool iVocDec::InitDecoder(int nCodecID, int nChannel, int nSampleRate)
{
	mpPkt = av_packet_alloc();

	mpCodec = avcodec_find_decoder(AVCodecID(nCodecID));
	if(!mpCodec)
	{
		// Log - "Codec not found"
		printf("[ERR] iVocDec::InitDecoder Codec not found");
		return false;
	}

	mpParser = av_parser_init(mpCodec->id);
	if(!mpParser)
	{
		// Log - "Parser not found"
		printf("[ERR] iVocDec::InitDecoder Parser not found");
		return false;
	}

	mpCtx = avcodec_alloc_context3(mpCodec);
	if(!mpCtx)
	{
		// Log - "Could not allocate audio codec context"
		printf("[ERR] iVocDec::InitDecoder Could not allocate audio codec context");
		return false;
	}

	mpCtx->channels = nChannel;
	mpCtx->sample_rate = nSampleRate;
	// mpCtx->sample_fmt = AV_SAMPLE_FMT_S16;

// printf("Context ID(%p) = %d / %d, Sample_Rate = %d / %d"
// 	, mpCtx, mpCtx->codec_id, AV_CODEC_ID_G723_1, mpCtx->sample_fmt, AV_SAMPLE_FMT_S16P);

	if(avcodec_open2(mpCtx, mpCodec, NULL) < 0)
	{
		// Log - "Open codec failed"
		printf("[ERR] iVocDec::InitDecoder Open codec failed");
		return false;
	}
	mpCtx->request_sample_fmt = AV_SAMPLE_FMT_U8;

	DispSampleRate(mpCodec);
	DispSampleFmt(mpCodec);
	DispChannelLayout(mpCodec);

	return true;
}

bool iVocDec::InitDecoderMp3(int nChannel, int nSampleRate)
{
	mstLame = lame_init();
	lame_set_num_channels(mstLame, nChannel);	// 1
	lame_set_VBR(mstLame, vbr_off);
	lame_set_brate(mstLame, 32);
	lame_set_in_samplerate(mstLame, nSampleRate);	// 8000
	lame_set_quality(mstLame, 2);
	if(lame_init_params(mstLame) < 0)
		return false;

	return true;
}

void iVocDec::FreeDecoder()
{
	avcodec_free_context(&mpCtx);
	av_parser_close(mpParser);
	av_frame_free(&mpFrame);
	av_packet_free(&mpPkt);
	Clear();
}

int32 iVocDec::Decode(const iByte_t* pSrc, int32 nSrc, iByte_t* pDst, int32 nDst, int32 nFmt)
{
	iData stData;
	while(nSrc>0)
	{
		if(mpFrame==NULL) mpFrame = av_frame_alloc();
		int32 nRet = av_parser_parse2(mpParser, mpCtx, &mpPkt->data, &mpPkt->size, pSrc, nSrc, AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
		if(nRet>0)
		{
			pSrc += nRet;
			nSrc -= nRet;
			if(mpPkt->size>0)
			{
				if(Decode(mpCtx, mpPkt, mpFrame, stData, nFmt)!=0)
				{
					// Error Log;
					printf("[ERR] iVocDec::Decode Decode Failed");
				}
			}
		}
	}
	if(stData.Size()>0)
	{
		nDst = IMIN(nDst, (int32)stData.Size());
		memcpy(pDst, stData.Bytes(), nDst);
		return nDst;
	}
	return 0;
}

void iVocDec::DispHelper()
{
	enum AVSampleFormat sFmt = mpCtx->sample_fmt;
	if (av_sample_fmt_is_planar(sFmt))
	{
		const char *packed = av_get_sample_fmt_name(sFmt);
		printf("Warning: the sample format the decoder produced is planar "
				 "(%s). This example will output the first channel only.", packed ? packed : "?");
		sFmt = av_get_packed_sample_fmt(sFmt);
	}
	int32 nChannels = mpCtx->channels;
	NPCSTR pFmt;
	GetFormatFromSampleFmt(&pFmt, sFmt);

	printf("Play the output audio file with the command:\n"
			 "\tffplay -f %s -ac %d -ar %d output_file", pFmt, nChannels, mpCtx->sample_rate);
}

int32 iVocDec::Decode(AVCodecContext* pCtx, AVPacket* pPkt, AVFrame* pFrame, iData& rData, int32 nFmt)
{
	int16 vPcm;
	int32 nRet = avcodec_send_packet(pCtx, pPkt);
	if(nRet<0)
	{
		printf("[ERR] iVocDec::Decod Error submitting the packet to the decoder");
		return nRet;
	}
	int32 nData = av_get_bytes_per_sample(pCtx->sample_fmt);
	if(nData<0)
	{
		// Log - "Failed to calculate data size"
		printf("[ERR] iVocDec::Decod Failed to calculate data size");
		return -1;
	}
	// printf("[INF] nData = %d", nData);
	while(nRet>=0)
	{
		nRet = avcodec_receive_frame(pCtx, pFrame);
		if(nRet==0)
		{
			for(int32 i=0; i<pFrame->nb_samples; i++)
			{
				for(int32 nChannel=0; nChannel<pCtx->channels; nChannel++)
				{
					switch(nFmt)
					{
					default:
					case WAVE_FORMAT_PCM:
						rData.AddBytes(pFrame->data[nChannel] + nData*i, nData);
						break;
					case WAVE_FORMAT_ALAW:
						vPcm = *(int16*)(pFrame->data[nChannel] + nData*i);
						rData.AddByte(Linear2aLaw(vPcm));
						break;
					case WAVE_FORMAT_MULAW:
						vPcm = *(int16*)(pFrame->data[nChannel] + nData*i);
						rData.AddByte(Linear2uLaw(vPcm));
						break;
					}
				}
			}
		}
		else if(nRet==AVERROR(EAGAIN) || nRet == AVERROR_EOF) return 0;
		else if(nRet<0)
		{
			// Log - "Error during decoding"
			printf("[ERR] iVocDec::Decod Error during decoding");
			return nRet;
		}
	}
	return 0;
}

void iVocDec::FlushDecoder()
{
	iData sData;
	mpPkt->data = NULL;
	mpPkt->size = 0;
	if(Decode(mpCtx, mpPkt, mpFrame, sData, WAVE_FORMAT_PCM)==0)
		printf("iVocDec::FlushDecoder FlushDecoder = Data = %lu", sData.Size());
}

void iVocDec::Clear()
{
	mpCodec = NULL;
	mpCtx = NULL;
	mpParser = NULL;
	mpPkt = NULL;
	mpFrame = NULL;
	lame_close(mstLame);

	memset(msBuf, 0x00, sizeof(msBuf));
	mnMP3Size = 0;
}

/* voc file -> pcm memory -> mp3 file */
int iVocDec::toMP3(short int* pSrc, int32 nSrc, iByte_t* pDst)
{
	int32 nWrite;
	if (nSrc == 0)
		nWrite = lame_encode_flush(mstLame, pDst, 9600);
	else
		nWrite = lame_encode_buffer(mstLame, pSrc, pSrc, nSrc, pDst, 9600);

	return nWrite;
}

/* voc file -> pcm file -> mp3 file */
// bool iVocDec::toMP3(NPCSTR szIn, NPCSTR szOut)
// {
// 	int nRead, nWrite;
// 	FILE* fpPcm = fopen(szIn, "rb");
// 	if(fpPcm == NULL)	return false;
// 	fseek(fpPcm, ILEN_4K, SEEK_CUR);
//
// 	FILE* fpMp3 = fopen(szOut, "wb");
// 	if(fpMp3 == NULL)	return false;
// 	const int PCM_SIZE = 9600;
// 	const int MP3_SIZE = 9600;
// 	short int pcm_buffer[PCM_SIZE];
// 	unsigned char mp3_buffer[MP3_SIZE];
//
// 	lame_t stLame = lame_init();
// 	lame_set_num_channels(stLame, 1);
// 	lame_set_VBR(stLame, vbr_off);
// 	lame_set_brate(stLame, 32);
// 	lame_set_in_samplerate(stLame, 8000);
// 	lame_set_quality(stLame, 2);
// 	if(lame_init_params(stLame) < 0)
// 		return false;
//
// 	int nTotalRead = 0;
// 	int nTotalWrite = 0;
//
// 	do {
// 		nRead = fread(pcm_buffer, sizeof(short int), PCM_SIZE, fpPcm);
//
// 		nTotalRead+=nRead;
//
// 		if (nRead == 0)
// 			nWrite = lame_encode_flush(stLame, mp3_buffer, MP3_SIZE);
// 		else
// 			nWrite = lame_encode_buffer(stLame, pcm_buffer,pcm_buffer, nRead, mp3_buffer, MP3_SIZE);
//
// 		fwrite(mp3_buffer, nWrite, 1, fpMp3);
// 		nTotalWrite += nWrite;
// 	} while (nRead != 0);
// 	mnMP3Size = nTotalWrite;
//
// 	lame_close(stLame);
// 	fclose(fpMp3);
// 	fclose(fpPcm);
//
// 	return true;
// }


















static int32 GetFormatFromSampleFmt(NPCSTR *ppFmt, enum AVSampleFormat eSampleFmt)
{
	struct SampleFmtEntry
	{
		enum AVSampleFormat nSampleFmt;
		const char *pFmtBE, *pFmtLE;
	} stSampleFmtEntries[] = {
		{ AV_SAMPLE_FMT_U8,  "u8",    "u8"    },
		{ AV_SAMPLE_FMT_S16, "s16be", "s16le" },
		{ AV_SAMPLE_FMT_S32, "s32be", "s32le" },
		{ AV_SAMPLE_FMT_FLT, "f32be", "f32le" },
		{ AV_SAMPLE_FMT_DBL, "f64be", "f64le" },
	};
	*ppFmt = NULL;
	for(int32 i=0; i<(int32)FF_ARRAY_ELEMS(stSampleFmtEntries); i++)
	{
		struct SampleFmtEntry *pEntry = &stSampleFmtEntries[i];
		if (eSampleFmt == pEntry->nSampleFmt)
		{
			*ppFmt = AV_NE(pEntry->pFmtBE, pEntry->pFmtLE);
			return 0;
		}
	}
	printf("[WRN] sample format %s is not supported as output format", av_get_sample_fmt_name(eSampleFmt));
	return -1;
}


static void DispSampleFmt(const AVCodec *pCodec)
{
	printf(">> %s %d", __FUNC__, __LINE__);
	const enum AVSampleFormat *pFmts = pCodec->sample_fmts;
	while(pFmts && *pFmts != AV_SAMPLE_FMT_NONE)
	{
		printf("[INF] %s - %d", __FUNC__, *pFmts);
		pFmts++;
	}
	printf("<< %s %d", __FUNC__, __LINE__);
}

static void DispSampleRate(const AVCodec* pCodec)
{
	printf(">> %s %d", __FUNC__, __LINE__);
	const int32 *pPos;
	if(pCodec->supported_samplerates != 0)
	{
		pPos = pCodec->supported_samplerates;
		while(*pPos)
		{
			printf("[INF] %s = %d", __FUNC__, *pPos);
			pPos++;
		}
	}
	printf("<< %s %d", __FUNC__, __LINE__);
}

static void DispChannelLayout(const AVCodec* pCodec)
{
	printf(">> %s %d", __FUNC__, __LINE__);
	const uint64_t *pPos;
	if(pCodec->channel_layouts != 0)
	{
		pPos = pCodec->channel_layouts;
		while(*pPos)
		{
			int32 nNbChannels = av_get_channel_layout_nb_channels(*pPos);
			printf("[INF] %s = %ld, %d", __FUNC__, *pPos, nNbChannels);
			pPos++;
		}
	}
	printf("<< %s %d", __FUNC__, __LINE__);
}




























