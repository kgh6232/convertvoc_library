
#include "iFile.h"
#include "iVocDec.h"
#include "iVocFile.h"
#include "iCnvtVoc.h"
#include "Common.h"


iCnvtVoc::iCnvtVoc()
{
	mszOutDir[0] = '\0';
	mnOutFileSize = 0;
}

iCnvtVoc::~iCnvtVoc()
{

}

void iCnvtVoc::Init(NPCSTR szOutDir)
{
	strcpy(mszOutDir, szOutDir);
	if(mszOutDir[strlen(mszOutDir)-1] == CH_PATH_SEPARATOR)
		mszOutDir[strlen(mszOutDir)-1] = '\0';
	mnOutFileSize = 0;
}

void iCnvtVoc::Clear()
{

}

int32 iCnvtVoc::ConvertVoc(NPCSTR szVocFile, uint16 nAudioType)
{
	uint32 nRc = CONVERT_VOC_RESULT_SUCCESS;

	// make output file path
	NPCSTR pStr = strrchr(szVocFile, CH_PATH_SEPARATOR);
	if(pStr == NULL)
		snprintf(mszOutFile, MAXLEN_LINE4, "%s%s%s", mszOutDir, SZ_PATH_SEPARATOR, szVocFile);
	else
		snprintf(mszOutFile, MAXLEN_LINE4, "%s%s%s", mszOutDir, SZ_PATH_SEPARATOR, pStr+1);

	// delete extention
	pStr = strrchr(mszOutFile, '.');
	if(pStr != NULL)	strcpy((char*)pStr, "\0");

	switch(nAudioType)
	{
		case CONVERT_VOC_AUDIO_TYPE_MP3 :
		{
			char szPCM[4096] = { 0 };
			sprintf(szPCM, "%s.for_mp3.wav", mszOutFile);
			strcat(mszOutFile, ".mp3");
			nRc = Voc2MP3(szVocFile, szPCM, mszOutFile);
			break;
		}
		case CONVERT_VOC_AUDIO_TYPE_PCM :
		{
			strcat(mszOutFile, ".wav");
			nRc = Voc2Pcm(szVocFile, mszOutFile);
			break;
		}
		case CONVERT_VOC_AUDIO_TYPE_ALAW :
		{
			strcat(mszOutFile, ".wav");
			nRc = Voc2Alaw(szVocFile, mszOutFile);
			break;
		}
		case CONVERT_VOC_AUDIO_TYPE_G723_1 :
		{
			strcat(mszOutFile, ".wav");
			nRc = DecodeVoc(szVocFile, mszOutFile);
			break;
		}
		default :
			nRc = CONVERT_VOC_RESULT_CODEC_ERROR;
	}

	return nRc;
}

/* voc file -> pcm memory -> mp3 file */
int32 iCnvtVoc::Voc2MP3(NPCSTR szIn, NPCSTR szPCM, NPCSTR szOut)
{
	iFile stOut;
	iVocDec stDec;
	iVocFile stVoc;
	int32 nTotal = 0;
	int32 nRc = CONVERT_VOC_RESULT_SUCCESS;

	if(!stDec.InitDecoderMp3(1, 8000))
	{	
		printf("Failed to init mp3 codec. (channel : %d | samplerate : %d)\n", 1, 8000);
		return CONVERT_VOC_RESULT_CANNOT_OPEN_VOC;
	}

	if(stVoc.OpenVoc(szIn))
	{
		if(stDec.InitDecoder(AV_CODEC_ID_G723_1, 1, 8000))
		{
			if(stOut.OpenFile(szOut, "wb"))
			{
				int32 nInBuf = 96, nOutBuf = 0, nOutMp3Buf = 0;
				iByte_t szInBuf[1920];
				iByte_t szOutBuf[9600];
				short int szPcmBuf[9600];
				iByte_t szOutMp3Buf[9600];

				memset(szInBuf, 0, 1920);
				memset(szOutBuf, 0, 9600);
				memset(szPcmBuf, 0, 9600);
				memset(szOutMp3Buf, 0, 9600);

				while(nInBuf>0)
				{
					nInBuf = stVoc.ReadVoc(szInBuf, nInBuf);
					nOutBuf = stDec.Decode(szInBuf, nInBuf, szOutBuf, sizeof(szOutBuf));
					memcpy(szPcmBuf, szOutBuf, nOutBuf);
					nOutMp3Buf = stDec.toMP3(szPcmBuf, nOutBuf/2, szOutMp3Buf);
					nTotal += stOut.Write((NPCSTR)szOutMp3Buf, nOutMp3Buf);
				}
				stDec.FlushDecoder();
				nOutMp3Buf = stDec.toMP3(NULL, 0, szOutMp3Buf);	// flush
				nTotal += stOut.Write((NPCSTR)szOutMp3Buf, nOutMp3Buf);
				
				mnOutFileSize = nTotal;
				nRc = CONVERT_VOC_RESULT_SUCCESS;
			}
			else
				nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_DST_FILE;

			stOut.CloseFile();
			stDec.FreeDecoder();
			stVoc.CloseVoc();
		}
		else
			nRc = CONVERT_VOC_RESULT_CANNOT_FIND_CODEC;
	}
	else
		nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_VOC;

	return nRc;
}

/* voc file -> pcm file -> mp3 file */
// int32 iCnvtVoc::Voc2MP3(NPCSTR szIn, NPCSTR szPCM, NPCSTR szOut)
// {
// 	printf("%s : %s : %s\n", szIn, szPCM, szOut);
// 	iVocDec stDec;
//
// 	int32 nRc = CONVERT_VOC_RESULT_SUCCESS;
// 	nRc = Voc2Pcm(szIn, szPCM);
// 	if (nRc != CONVERT_VOC_RESULT_SUCCESS)
// 	{
// 		printf("Failed to convert pcm\n");
// 		return nRc;
// 	}
// 	if(!stDec.toMP3(szPCM, szOut))
// 	{
// 		printf("Failed to convert wav to mp3\n");
// 		nRc = CONVERT_VOC_RESULT_CONVERT_FAIL;
// 	}
// 	if (remove(szPCM) < 0)
// 		printf("Failed to remove temporary file.\n");
// 	mnOutFileSize = stDec.GetMp3Size();
//
// 	return nRc;
// }

int32 iCnvtVoc::Voc2Pcm(NPCSTR szIn, NPCSTR szOut)
{
	iFile stOut;
	iVocDec stDec;
	iVocFile stVoc;
	int32 nTotal = 0;
	int32 nRc = CONVERT_VOC_RESULT_SUCCESS;

	WAVE_HEADER_PCM stHdr = {
		{'R','I','F','F'}, 0, {'W','A','V','E'}, {'f','m','t',' '}, sizeof(stHdr.stWaveFmt),
		{WAVE_FORMAT_PCM, AUDIO_CHANNEL_MONO, 8000, 16000, 2, 16},
		{'f','a','c','t'}, 4, 0,
		{'d','a','t','a'}, 0};

	if(stVoc.OpenVoc(szIn))
	{
		if(stDec.InitDecoder(AV_CODEC_ID_G723_1, 1, 8000))
		{
			if(stOut.OpenFile(szOut, "wb"))
			{
				stOut.Write((NPCSTR)&stHdr, sizeof(stHdr));
				int32 nInBuf = 96, nOutBuf = 0;
				iByte_t szInBuf[1920];
				iByte_t szOutBuf[9600*2];

				while(nInBuf>0)
				{
					memset(szInBuf, 0, 1920);
					memset(szOutBuf, 0, 9600*2);
				
					nInBuf = stVoc.ReadVoc(szInBuf, nInBuf);
					nOutBuf = stDec.Decode(szInBuf, nInBuf, szOutBuf, sizeof(szOutBuf));
					nTotal += stOut.Write((NPCSTR)szOutBuf, nOutBuf);
				}
				stDec.FlushDecoder();

				stHdr.stWaveFmt.wBitsPerSample = 16;
				stHdr.stWaveFmt.nSamplesPerSec = stDec.Ctx()->sample_rate;
				stHdr.stWaveFmt.nAvgBytesPerSec = stHdr.stWaveFmt.nSamplesPerSec
					* stHdr.stWaveFmt.nChannels * (stHdr.stWaveFmt.wBitsPerSample/8);

				stHdr.dwFileSize = nTotal+(sizeof(stHdr)-sizeof(stHdr.szFileFormat)-sizeof(stHdr.dwFileSize));
				stHdr.dwDataSize = nTotal;

				printf("Total = %d, header = %lu", nTotal, sizeof(stHdr));
				printf("wBitsPerSample = %d, nSamplesPerSec = %d, nAvgBytesPerSec = %d"
					, stHdr.stWaveFmt.wBitsPerSample, stHdr.stWaveFmt.nSamplesPerSec, stHdr.stWaveFmt.nAvgBytesPerSec);

				stOut.Seek(0, iFile::IFILE_BEGIN);
				stOut.Write((NPCSTR)&stHdr, sizeof(stHdr));

				mnOutFileSize = stHdr.dwFileSize;
				nRc = CONVERT_VOC_RESULT_SUCCESS;
			}
			else
				nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_DST_FILE;
			stDec.DispHelper();
			stOut.CloseFile();
			stDec.FreeDecoder();
			stVoc.CloseVoc();
		}
		else
			nRc = CONVERT_VOC_RESULT_CANNOT_FIND_CODEC;
	}
	else
		nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_VOC;

	return nRc;
}

int32 iCnvtVoc::Voc2Alaw(NPCSTR szIn, NPCSTR szOut)
{
	iFile stOut;
	iVocDec stDec;
	iVocFile stVoc;
	int32 nTotal = 0;
	int32 nRc = CONVERT_VOC_RESULT_SUCCESS;

	WAVE_HEADER_PCM stHdr = {
		{'R','I','F','F'}, 0, {'W','A','V','E'}, {'f','m','t',' '}, sizeof(stHdr.stWaveFmt),
		{ WAVE_FORMAT_ALAW, AUDIO_CHANNEL_MONO, 8000, 8000, 1, 8},
		{'f','a','c','t'}, 4, 0,
		{'d','a','t','a'}, 0};

	if(stVoc.OpenVoc(szIn))
	{
		if(stDec.InitDecoder(AV_CODEC_ID_G723_1, 1, 8000))
		{
			if(stOut.OpenFile(szOut, "wb"))
			{
				stOut.Write((NPCSTR)&stHdr, sizeof(stHdr) != sizeof(stHdr));
				int32 nInBuf = 96, nOutBuf = 0;
				iByte_t szInBuf[256];
				iByte_t szOutBuf[sizeof(szInBuf)*16];

				while(nInBuf>0)
				{
					memset(szInBuf, 0, 256);
					memset(szOutBuf, 0, sizeof(szInBuf)*16);

					nInBuf = stVoc.ReadVoc(szInBuf, nInBuf);
					nOutBuf = stDec.Decode(szInBuf, nInBuf, szOutBuf, sizeof(szOutBuf), WAVE_FORMAT_ALAW);
					nTotal += stOut.Write((NPCSTR)szOutBuf, nOutBuf);
				}
				stDec.FlushDecoder();

				stHdr.stWaveFmt.wBitsPerSample = 8;
				stHdr.stWaveFmt.nSamplesPerSec = 8000;
				stHdr.stWaveFmt.nAvgBytesPerSec = stHdr.stWaveFmt.nSamplesPerSec * stHdr.stWaveFmt.nChannels * (stHdr.stWaveFmt.wBitsPerSample/8);
				stHdr.dwFileSize = nTotal+48;
				stHdr.dwDataSize = nTotal;

				printf("Total = %d, header = %lu", nTotal, sizeof(stHdr));
				printf("wBitsPerSample = %d, nSamplesPerSec = %d, nAvgBytesPerSec = %d"
						, stHdr.stWaveFmt.wBitsPerSample, stHdr.stWaveFmt.nSamplesPerSec, stHdr.stWaveFmt.nAvgBytesPerSec);

				stOut.Seek(0, iFile::IFILE_BEGIN);
				stOut.Write((NPCSTR)&stHdr, sizeof(stHdr));

				mnOutFileSize = stHdr.dwFileSize;
				nRc = CONVERT_VOC_RESULT_SUCCESS;
			}
			else
				nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_DST_FILE;

			stDec.DispHelper();
			stOut.CloseFile();
			stDec.FreeDecoder();
			stVoc.CloseVoc();
		}
		else
			nRc = CONVERT_VOC_RESULT_CANNOT_FIND_CODEC;
	}
	else
		nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_VOC;

	return nRc;
}

int32 iCnvtVoc::DecodeVoc(NPCSTR szIn, NPCSTR szOut)
{
	iFile stOut;
	iVocFile stVoc;
	int32 nTotal = 0;
	int32 nRc = CONVERT_VOC_RESULT_SUCCESS;

	WAVE_HEADER_PCM stHdr = {
		{'R','I','F','F'}, 0, {'W','A','V','E'}, {'f','m','t',' '}, sizeof(stHdr.stWaveFmt),
		{ 66, AUDIO_CHANNEL_MONO, 8000, 800, 24, 0},
		{'f','a','c','t'}, 4, 0,
		{'d','a','t','a'}, 0};

	if(stVoc.OpenVoc(szIn))
	{
		if(stOut.OpenFile(szOut, "wb"))
		{
			stOut.Write((NPCSTR)&stHdr, sizeof(stHdr));
			int32 nInBuf = 96;
			iByte_t szInBuf[1920];

			while(nInBuf>0)
			{
				memset(szInBuf, 0, 1920);

				nInBuf = stVoc.ReadVoc(szInBuf, nInBuf);
				nTotal += stOut.Write((NPCSTR)szInBuf, nInBuf);
			}
			stHdr.stWaveFmt.wBitsPerSample = 0;
			stHdr.stWaveFmt.nSamplesPerSec = 8000;
			stHdr.stWaveFmt.nAvgBytesPerSec = 800;
			stHdr.stWaveFmt.nBlockAlign = 24;

			stHdr.dwFileSize = nTotal+48;
			stHdr.dwDataSize = nTotal;

			printf("Total = %d, header = %lu", nTotal, sizeof(stHdr));
			printf("wBitsPerSample = %d, nSamplesPerSec = %d, nAvgBytesPerSec = %d"
				, stHdr.stWaveFmt.wBitsPerSample, stHdr.stWaveFmt.nSamplesPerSec, stHdr.stWaveFmt.nAvgBytesPerSec);

			stOut.Seek(0, iFile::IFILE_BEGIN);
			stOut.Write((NPCSTR)&stHdr, sizeof(stHdr));

			mnOutFileSize = stHdr.dwFileSize;
			nRc = CONVERT_VOC_RESULT_SUCCESS;
		}
		else
			nRc = CONVERT_VOC_RESULT_CANNOT_FIND_CODEC;
		stOut.CloseFile();
		stVoc.CloseVoc();
	}
	else
		nRc = CONVERT_VOC_RESULT_CANNOT_OPEN_VOC;

	return nRc;
}








