//
//  iObj.cpp
//  iCore
//
//  Created by 류관중 on 12. 5. 14.
//  Copyright (c) 2012. Bridgetec. All rights reserved.
//

#include "iObj.h"
iSize_t theObjCnt = 0;

#ifdef IWINDOWS
CRITICAL_SECTION theObjCs;
#else
#define EnterCriticalSection        pthread_mutex_lock
#define LeaveCriticalSection        pthread_mutex_unlock
pthread_mutex_t theObjCs = PTHREAD_MUTEX_INITIALIZER;
#endif//IWINDOWS

iObj::iObj()
{
	ISETNAME;
#ifdef IWINDOWS
	if(theObjCnt==0) InitializeCriticalSection(&theObjCs);
#endif//IWINDOWS
	mpObject = NULL;
	EnterCriticalSection(&theObjCs);
	++theObjCnt;
	LeaveCriticalSection(&theObjCs);
}

iObj::~iObj()
{
	EnterCriticalSection(&theObjCs);
	--theObjCnt;
	LeaveCriticalSection(&theObjCs);
#ifdef IWINDOWS
	if(theObjCnt==0) DeleteCriticalSection(&theObjCs);
#endif//IWINDOWS
}

iSize_t iObj::ObjCnt()
{
	return theObjCnt;
}

NPCSTR iObj::ObjName()
{
	return mszObjName;
}

void iObj::setObjName(NPCSTR szFmt, ...)
{
	memset(mszObjName, 0x00, sizeof(mszObjName));
	va_list ap;
	va_start(ap, szFmt);
	vsnprintf(mszObjName, sizeof(mszObjName), szFmt, ap);
	va_end(ap);
}
