.SUFFIXES		: .cpp .o

CC				= gcc
CXX				= $(CC)

##########################################
# Adjustment for Build Machines OS/Bits  #
#----------------------------------------#
GET_OS			= $(shell uname -s)
GET_BIT			= $(shell uname -m)

ifneq (, $(findstring Linux, $(GET_OS)))
MACH_OS			= LINUX
ifneq (, $(findstring x86_64, $(GET_BIT)))
MACH_BIT		= "64"
MACH_OUT		= l64
else
MACH_BIT		= "32"
MACH_OUT		= l32
endif
endif

ifneq (, $(findstring SunOS, $(GET_OS)))
MACH_OS			= SOLARIS
GET_BIT			= $(shell isainfo -b)
ifneq (, $(findstring 64, $(GET_BIT)))
MACH_BIT		= "64"
MACH_OUT		= s64
CC				= gcc -m64
CXX				= $(CC)
else
MACH_BIT		= "32"
MACH_OUT		= s32
endif
endif

ifneq (, $(findstring Darwin, $(GET_OS)))
MACH_OS			= MACOSX
ifneq (, $(findstring x86_64, $(GET_BIT)))
MACH_BIT		= "64"
MACH_OUT		= m64
else
MACH_BIT		= "32"
MACH_OUT		= m32
endif
endif

##########################################
# Source Directory & Environment Setting #
#----------------------------------------#
ifneq (, $(findstring Linux, $(GET_OS)))
FFMPEG_INC		= ./ffmpeg_lib/linux/inc
FFMPEG_LIB		= ./ffmpeg_lib/linux/lib
else ifneq (, $(findstring Darwin, $(GET_OS)))
FFMPEG_INC		= ./ffmpeg_lib/mac/inc
FFMPEG_LIB		= ./ffmpeg_lib/mac/lib
endif

#####################################
# Copy/Moving files when installing #
#-----------------------------------#
INC_FILES		= iCnvtVoc.h
INC_CPATH		= 
LIB_FILES		= $(TARGET)
LIB_CPATH		= 
BIN_FILES		=
BIN_CPATH		=

###################################
# Include & Library Paths Setting #
#---------------------------------#
INC_PATHS		= -I$(FFMPEG_INC)
LIB_PATHS		= -L$(FFMPEG_LIB) -L./

##########################################
# Define / Compile flags / Linking flags #
#----------------------------------------#
DEFINE			= -D$(MACH_OS) -D$(MACH_OUT)
DEFINE			+= 
CFLAGS			= -g -Wall -fPIC
LFLAGS			= -lstdc++ -lpthread 
LFLAGS			+= -Wl,-Bsymbolic -lavformat -lavcodec -lswscale -lavutil -lswresample -lm -lz
LFLAGT			= -liCnvtVoc -lmp3lame -lopus -lfdk-aac

############################
# Unit Testing for library #
#--------------------------#
UTSRC			= iTest.cpp
# UTSRC			= $(subst iMain.cpp,,$(wildcard *.cpp))
UTOBJ			= $(UTSRC:.cpp=.o)
UTBIN			= iTest

############################
# Main Sources and Objects #
# (Edit your TARGET name.) #
#--------------------------#
SRCS			= $(subst iTest.cpp,,$(wildcard *.cpp))
OBJS			= $(SRCS:.cpp=.o)
TARGET			= libiCnvtVoc.so
ifdef IRELEASE
INSTALLS		= $(TARGET)
else
DEFINE 			+= -DIDEBUG
INSTALLS		= $(TARGET) $(UTBIN)
endif

##################################################
# Linking Part                                   #
# (Select linking option on your TARGET purpose) #
#------------------------------------------------#
all				: $(INSTALLS)
$(TARGET)		: $(OBJS)
# 				@$(AR) -r $(TARGET) $(OBJS)
				@$(CC) -shared -o $(TARGET) $(OBJS) $(LFLAGS) $(LIB_PATHS)
# 				@$(CC) -o $(TARGET) $(OBJS) $(LFLAGS) $(LIB_PATHS)
				@echo "==============================================================="
				@echo "[35m$(TARGET) Compile Complete !![0m"








##########################
# DON'T edit below lines #
#------------------------#
$(UTBIN)		: $(UTOBJ)
				@$(CC) -o $(UTBIN) $(UTOBJ) $(LFLAGS) $(LFLAGT) $(LIB_PATHS)
				@echo "==============================================================="
				@echo "[35m$(UTBIN) Compile Complete !![0m"

.cpp.o			:
				@echo "[$(TARGET)] Compiling [33m$<[0m ..."
				@$(CC) $(CFLAGS) -c $< -o $@ $(INC_PATHS) $(DEFINE) $(PKGDEF)

install			: $(INSTALLS)
ifdef BIN_FILES
ifdef BIN_CPATH
				@mkdir -p $(BIN_CPATH)
				@mv $(BIN_FILES) $(BIN_CPATH)/
				@echo "[$(TARGET)] [33mMove Binary Files($(BIN_FILES)) to $(BIN_CPATH)[0m ..."
endif
endif
ifdef INC_FILES
ifdef INC_CPATH
				@mkdir -p $(INC_CPATH)
				@cp $(INC_FILES) $(INC_CPATH)/
				@echo "[$(TARGET)] [33mCopy Include Files($(INC_FILES)) to $(INC_CPATH)[0m ..."
endif
endif
ifdef LIB_FILES
ifdef LIB_CPATH
				@mkdir -p $(LIB_CPATH)
				@cp -d $(LIB_FILES) $(LIB_CPATH)/
				@echo "[$(TARGET)] [33mCopy Library Files($(LIB_FILES)) to $(LIB_CPATH)[0m ..."
endif
endif
				@echo "==============================================================="
				@echo "[34m$(TARGET) Install Complete !![0m"

target			: $(TARGET)
ifdef BIN_FILES
ifdef BIN_CPATH
				@mkdir -p $(BIN_CPATH)
				@mv $(BIN_FILES) $(BIN_CPATH)/
				@echo "[$(TARGET)] [33mMove Binary Files($(BIN_FILES)) to $(BIN_CPATH)[0m ..."
endif
endif
ifdef INC_FILES
ifdef INC_CPATH
				@mkdir -p $(INC_CPATH)
				@cp $(INC_FILES) $(INC_CPATH)/
				@echo "[$(TARGET)] [33mCopy Include Files($(INC_FILES)) to $(INC_CPATH)[0m ..."
endif
endif
ifdef LIB_FILES
ifdef LIB_CPATH
				@mkdir -p $(LIB_CPATH)
				@cp -d $(LIB_FILES) $(LIB_CPATH)/
				@echo "[$(TARGET)] [33mCopy Library Files($(LIB_FILES)) to $(LIB_CPATH)[0m ..."
endif
endif
				@echo "==============================================================="
				@echo "[34m$(TARGET) Install Complete !![0m"

dep				:
				@gccmakedep $(INC_PATHS) $(SRCS)
				@echo "# Make Source Dependencies Success !!"

clean			:
				@echo "[$(TARGET)] Delete Object Files !!"
				@$(RM) $(OBJS) $(TARGET) $(UTOBJ) $(UTBIN) *.log *.wav *.mp3










