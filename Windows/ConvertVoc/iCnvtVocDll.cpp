
#define DLLEXPORT

#include "iCnvtVocDll.h"
#include "iCnvtVoc.h"

extern "C" MYDLLTYPE void* iCnvtVocConstruct()
{
	iCnvtVoc* pCnvtVoc = new iCnvtVoc();
	return (NPVOID)pCnvtVoc;
}

extern "C" MYDLLTYPE void iCnvtVocInit(void* pObj, NPCSTR szOutDir)
{
	iCnvtVoc* pCnvtVoc = (iCnvtVoc*)pObj;
	pCnvtVoc->Init(szOutDir);
}

extern "C" MYDLLTYPE void iCnvtVocClear(void* pObj)
{
	iCnvtVoc* pCnvtVoc = (iCnvtVoc*)pObj;
	pCnvtVoc->Clear();
}

extern "C" MYDLLTYPE int32 iCnvtVocConvertVoc(void* pObj, NPCSTR szVocFile, uint16 nAudioType)
{
	iCnvtVoc* pCnvtVoc = (iCnvtVoc*)pObj;
	return pCnvtVoc->ConvertVoc(szVocFile, nAudioType);
}

extern "C" MYDLLTYPE NPCSTR iCnvtVocGetOutFilePath(void* pObj)
{
	iCnvtVoc* pCnvtVoc = (iCnvtVoc*)pObj;
	return pCnvtVoc->GetOutFilePath();
}

extern "C" MYDLLTYPE uint64 iCnvtVocGetOutFileSize(void* pObj)
{
	iCnvtVoc* pCnvtVoc = (iCnvtVoc*)pObj;
	return pCnvtVoc->GetOutFileSize();
}