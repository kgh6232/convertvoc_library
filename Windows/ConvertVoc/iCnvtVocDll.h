
#ifdef DLLEXPORT
#define MYDLLTYPE __declspec(dllexport)
#else
#define MYDLLTYPE __declspec(dllexport)
#endif

#include "iType.h"
#include "Common.h"

extern "C" MYDLLTYPE NPVOID iCnvtVocConstruct();
extern "C" MYDLLTYPE void iCnvtVocInit(void* pObj, NPCSTR szOutDir);
extern "C" MYDLLTYPE void iCnvtVocClear(void* pObj);
extern "C" MYDLLTYPE int32 iCnvtVocConvertVoc(void* pObj, NPCSTR szVocFile, uint16 nAudioType = CONVERT_VOC_AUDIO_TYPE_MP3);
extern "C" MYDLLTYPE NPCSTR iCnvtVocGetOutFilePath(void* pObj);
extern "C" MYDLLTYPE uint64 iCnvtVocGetOutFileSize(void* pObj);