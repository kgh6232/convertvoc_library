
#include "iCnvtVocDll.h"
#include "Common.h"

int main()
{
	NPVOID pCnvtVoc = iCnvtVocConstruct();

	int32 nRc;

	//iCnvtVocInit(pCnvtVoc, ".\\");
	//nRc = iCnvtVocConvertVoc(pCnvtVoc, "_input.voc", CONVERT_VOC_AUDIO_TYPE_PCM);
	//printf("** Result   : %d\n", nRc);
	//printf("** File     : %s\n", iCnvtVocGetOutFilePath(pCnvtVoc));
	//printf("** FileSize : %lld\n", iCnvtVocGetOutFileSize(pCnvtVoc));
	//iCnvtVocClear(pCnvtVoc);

	iCnvtVocInit(pCnvtVoc, ".\\");
	nRc = iCnvtVocConvertVoc(pCnvtVoc, "_input.voc", CONVERT_VOC_AUDIO_TYPE_MP3);
	printf("** Result   : %d\n", nRc);
	printf("** File     : %s\n", iCnvtVocGetOutFilePath(pCnvtVoc));
	printf("** FileSize : %lld\n", iCnvtVocGetOutFileSize(pCnvtVoc));
	iCnvtVocClear(pCnvtVoc);

	return 0;
}