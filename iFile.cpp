//
//  iFile.cpp
//  iCore
//
//  Created by 류관중 on 12. 5. 14..
//  Copyright (c) 2012. Bridgetec. All rights reserved.
//

#include "iFile.h"

#ifdef IWINDOWS
#pragma warning (disable:4244)
#endif

iFile::iFile()
{
	ISETNAME;
	mpFile = NULL;
	memset(mszFile, 0x00, sizeof(mszFile));
}

iFile::~iFile()
{
	CloseFile();
}

bool iFile::OpenFile(NPCSTR szFile, NPCSTR szOpt)
{
	if(mpFile) CloseFile();
	mpFile = fopen(szFile, szOpt);
	if(!mpFile) return false;
	strcpy(mszFile, szFile);
	OnFileOpen(mpFile, mszFile);
	return true;
}

void iFile::CloseFile()
{
	if(mpFile)
	{
		OnFileClose(mpFile, mszFile);
		fclose(mpFile);
		mpFile = NULL;
		memset(mszFile, 0x00, sizeof(mszFile));
	}
}

iSize_t iFile::Read(NPSTR szBuf, iSize_t nSize)
{
	iSize_t nRet = 0;
	if(mpFile) nRet = fread(szBuf, sizeof(char), nSize, mpFile);
	return nRet;
}

iSize_t iFile::Write(NPCSTR szBuf, iSize_t nSize)
{
    iSize_t nRet = 0;
	if(mpFile) nRet = fwrite(szBuf, sizeof(char), nSize, mpFile);
	return nRet;
}

bool iFile::Flush()
{
	bool bRet = false;
	if(mpFile && fflush(mpFile)==0) bRet = true;
	return bRet;
}

int32 iFile::GetC()
{
	int32 nRet = -1;
	if(mpFile) nRet = fgetc(mpFile);
	return nRet;
}

NPSTR iFile::GetS(NPSTR szBuf, iSize_t nSize)
{
	NPSTR pRet = NULL;
	if(mpFile) pRet = fgets(szBuf, nSize, mpFile);
	return pRet;
}

bool iFile::PutC(int32 nChar)
{
	bool bRet = false;
	if(mpFile && fputc(nChar, mpFile)!=EOF) bRet = true;
	return bRet;
}

bool iFile::PutS(NPCSTR szBuf)
{
	bool bRet = false;
	if(mpFile && fputs(szBuf, mpFile)!=EOF) bRet = true;
	return bRet;
}

bool iFile::Seek(isSize_t nOffset, eFileOrig nOrigin)
{
	bool bRet = false;
	if(mpFile && fseek(mpFile, nOffset, nOrigin)==0) bRet = true;
	return bRet;
}

isSize_t iFile::Tell()
{
	isSize_t nRet = -1;
	if(mpFile) nRet = ftell(mpFile);
	return nRet;
}

iSize_t iFile::GetSize()
{
#ifdef IWINDOWS
	struct _stat wfd;
	if(_stat(mszFile, &wfd)==0) return (iSize_t)wfd.st_size;
#else
	struct stat wfd;
	if(stat(mszFile, &wfd)==0) return (iSize_t)wfd.st_size;
#endif
	return 0;
}

int32 iFile::Printf(NPCSTR szFmt, ...)
{
	int32 nRet = -1;
	if(mpFile)
	{
		va_list pArg;
		va_start(pArg, szFmt);
		nRet = vfprintf(mpFile, szFmt, pArg);
		va_end(pArg);
	}
	return nRet;
}
