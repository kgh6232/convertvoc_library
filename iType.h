//
//  iType.h
//  iCore
//
//  Created by 류관중 on 12. 5. 14.
//  Copyright (c) 2012. Bridgetec. All rights reserved.
//

/**
 * @file    iType.h
 * @brief   iCORE용 Variable Type 정의 및 OS 별 Define
 *
 * @version v0.1
 * @see     None
 * @date    2012. 5. 14.
 * @warning 아래 Definition들을 사용하려고 한다면
 *          Definition을 사용하는곳 위에 iType.h를 include 하여야 함
 */

#pragma once


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>

// Compiler Check
#if (!defined (__GNUC__) && !defined (_MSC_VER) && !defined (__INTEL_COMPILER))
#error "Support GCC, MS Visual Studio, Xcode"
#endif


#if (defined (__ia64__) || defined (__IA64__) || defined (_M_IA64) || defined (__LP64__) || defined(_WIN64) || defined(__amd64) || defined(__amd64__))
#define N64BITS									//< 64bit 시스템.
#else
#define N32BITS									//< 32bit 시스템.
#endif


#if (defined (linux) || defined (__linux))
# define IUNIX									//< Unix 계열 OS
# define ILINUX									//< Linux 계열 OS
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>


#elif (defined (_WIN32) || defined (_WIN64))
# define IWINDOWS								//< Windows 계열 OS
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <Winsock2.h>
#include <WS2tcpip.h>

#elif (defined (__APPLE__))
# define IUNIX									//< Unix 계열 OS
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE
# define IPHONEOS								//< IOS 계열 OS
#elif TARGET_OS_MAC
# define IMACOSX								//< MAC OS X 계열 OS
#else
#error "Cannot decide the target operating systems. !!!"
#endif
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>


#elif (defined (sun) || defined(__sun))
# define IUNIX									//< Unix 계열 OS
# define ISOLARIS								//< Solaris OS
#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>


#else
#error "Cannot decide the target operating systems. !!!"
#endif


typedef signed char				int8;			//< 8bit 정수형 Type 재정의.
typedef unsigned char			uint8;			//< 8bit 자연수 Type 재정의.
typedef unsigned char			uchar;			//< unsigned char형 Type 재정의.
typedef signed short			int16;			//< 16bit 정수형 Type 재정의.
typedef unsigned short			uint16;			//< 16bit 자연수 Type 재정의.
typedef signed int				int32;			//< 32bit 정수형 Type 재정의.
typedef unsigned int			uint32;			//< 32bit 자연수 Type 재정의.


/******************************************
 * ILINUX Type and Macro Definitions
 ******************************************/
#if defined ILINUX
typedef signed long long		int64;			//< 64bit 정수형 Type 재정의.
typedef unsigned long long		uint64;			//< 64bit 자연수 Type 재정의.
typedef socklen_t				iSockLen_t;		//< OS 별로 사용되는 Socket Length 변수형 재정의.
typedef size_t					iSize_t;		//< OS 별로 사용되는 size_t(자연수) 변수형 재정의.
typedef ssize_t					isSize_t;		//< OS 별로 사용되는 ssize_t(정수) 변수형 재정의.
typedef int32					iSem_t;			//< OS 별로 사용되는 Semaphore 변수형 재정의.
typedef int32					iShm_t;			//< OS 별로 사용되는 Shared Memory 변수형 재정의.
typedef key_t 					iKey_t;			//< OS 별로 사용되는 key_t 변수형 재정의.
typedef pthread_t				iThread_t;		//< OS 별로 사용되는 Thread 변수형 재정의.
typedef pthread_cond_t			iEvent_t;		//< OS 별로 사용되는 Event 변수형 재정의.
typedef pthread_mutex_t			iMutex_t;		//< OS 별로 사용되는 Mutex 변수형 재정의.
typedef pthread_rwlock_t		iRwLock_t;		//< OS 별로 사용되는 ReadWrite Lock 변수형 재정의.
typedef pid_t					iPid_t;			//< OS 별로 사용되는 Process Id 변수형 재정의.
typedef sa_family_t				iFamily_t;		//< OS 별로 사용되는 Address Family 변수형 재정의.
typedef unsigned long			dword;			//< OS 별로 사용되는 DWORD(자연수) 변수형 재정의.

#define IEOLTYPE				("\n")
#define ITIMEDOUT				ETIMEDOUT
#define ILIB_INFINITE			(-1)
#define ILIBAPI


/******************************************
 * IWINDOWS Type and Macro Definitions
 ******************************************/
#elif defined IWINDOWS
typedef signed __int64			int64;			//< 64bit 정수형 Type 재정의.
typedef unsigned __int64		uint64;			//< 64bit 자연수 Type 재정의.
typedef socklen_t				iSockLen_t;		//< OS 별로 사용되는 Socket Length 변수형 재정의.
typedef SIZE_T					iSize_t;		//< OS 별로 사용되는 size_t(자연수) 변수형 재정의.
typedef SSIZE_T					isSize_t;		//< OS 별로 사용되는 ssize_t(정수) 변수형 재정의.
typedef HANDLE					iSem_t;			//< OS 별로 사용되는 Semaphore 변수형 재정의.
typedef HANDLE					iShm_t;			//< OS 별로 사용되는 Shared Memory 변수형 재정의.
typedef int32 					iKey_t;			//< OS 별로 사용되는 key_t 변수형 재정의.
typedef HANDLE					iThread_t;		//< OS 별로 사용되는 Thread 변수형 재정의.
typedef HANDLE					iEvent_t;		//< OS 별로 사용되는 Event 변수형 재정의.
typedef CRITICAL_SECTION		iMutex_t;		//< OS 별로 사용되는 Mutex 변수형 재정의.
typedef struct
{
	iMutex_t mCsRd;
	iMutex_t mCsWr;
	iEvent_t mEvt;
	int32 mRd;
} iRwLock_t;									//< OS 별로 사용되는 ReadWrite Lock 변수형 재정의.
typedef DWORD					iPid_t;			//< OS 별로 사용되는 Process Id 변수형 재정의.
typedef int16					iFamily_t;		//< OS 별로 사용되는 Address Family 변수형 재정의.
typedef unsigned long			dword;			//< OS 별로 사용되는 DWORD(자연수) 변수형 재정의.

#define IEOLTYPE				("\n")
#define ITIMEDOUT				WAIT_TIMEOUT
#define ILIB_INFINITE			(INFINITE)
#ifdef _ICORE_EXPORTS
	#define ILIBAPI			__declspec(dllexport)
#elif _ICORE_IMPORTS
	#define ILIBAPI			__declspec(dllimport)
#else
	#define ILIBAPI
#endif


#ifndef snprintf
#define snprintf				_snprintf
#endif
#ifndef strcasecmp
#define strcasecmp				_stricmp
#endif
#ifndef strncasecmp
#define strncasecmp				_strnicmp
#endif
#ifndef va_copy
#define va_copy(d,s)			((d)=(s))
#endif
#ifndef S_ISREG
#define S_ISREG(x)				((x)&_S_IFREG)
#endif
#ifndef S_ISDIR
#define S_ISDIR(x)				((x)&_S_IFDIR)
#endif



/******************************************
 * IMACOSX Type and Macro Definitions
 ******************************************/
#elif defined IMACOSX
typedef signed long long		int64;			//< 64bit 정수형 Type 재정의.
typedef unsigned long long		uint64;			//< 64bit 자연수 Type 재정의.
typedef socklen_t				iSockLen_t;		//< OS 별로 사용되는 Socket Length 변수형 재정의.
typedef size_t					iSize_t;		//< OS 별로 사용되는 size_t(자연수) 변수형 재정의.
typedef ssize_t					isSize_t;		//< OS 별로 사용되는 ssize_t(정수) 변수형 재정의.
typedef int32					iSem_t;			//< OS 별로 사용되는 Semaphore 변수형 재정의.
typedef int32					iShm_t;			//< OS 별로 사용되는 Shared Memory 변수형 재정의.
typedef key_t 					iKey_t;			//< OS 별로 사용되는 key_t 변수형 재정의.
typedef pthread_t				iThread_t;		//< OS 별로 사용되는 Thread 변수형 재정의.
typedef pthread_cond_t			iEvent_t;		//< OS 별로 사용되는 Event 변수형 재정의.
typedef pthread_mutex_t			iMutex_t;		//< OS 별로 사용되는 Mutex 변수형 재정의.
typedef pthread_rwlock_t		iRwLock_t;		//< OS 별로 사용되는 ReadWrite Lock 변수형 재정의.
typedef pid_t					iPid_t;			//< OS 별로 사용되는 Process Id 변수형 재정의.
typedef sa_family_t				iFamily_t;		//< OS 별로 사용되는 Address Family 변수형 재정의.
typedef unsigned long			dword;			//< OS 별로 사용되는 DWORD(자연수) 변수형 재정의.

#define IEOLTYPE				("\n")
#define ITIMEDOUT				ETIMEDOUT
#define ILIB_INFINITE			(-1)
#define ILIBAPI


/******************************************
 * IPHONEOS Type and Macro Definitions
 ******************************************/
#elif defined IPHONEOS
typedef signed long long		int64;			//< 64bit 정수형 Type 재정의.
typedef unsigned long long		uint64;			//< 64bit 자연수 Type 재정의.
typedef socklen_t				iSockLen_t;		//< OS 별로 사용되는 Socket Length 변수형 재정의.
typedef size_t					iSize_t;		//< OS 별로 사용되는 size_t(자연수) 변수형 재정의.
typedef ssize_t					isSize_t;		//< OS 별로 사용되는 ssize_t(정수) 변수형 재정의.
typedef int32					iSem_t;			//< OS 별로 사용되는 Semaphore 변수형 재정의.
typedef int32					iShm_t;			//< OS 별로 사용되는 Shared Memory 변수형 재정의.
typedef key_t 					iKey_t;			//< OS 별로 사용되는 key_t 변수형 재정의.
typedef pthread_t				iThread_t;		//< OS 별로 사용되는 Thread 변수형 재정의.
typedef pthread_cond_t			iEvent_t;		//< OS 별로 사용되는 Event 변수형 재정의.
typedef pthread_mutex_t			iMutex_t;		//< OS 별로 사용되는 Mutex 변수형 재정의.
typedef pthread_rwlock_t		iRwLock_t;		//< OS 별로 사용되는 ReadWrite Lock 변수형 재정의.
typedef pid_t					iPid_t;			//< OS 별로 사용되는 Process Id 변수형 재정의.
typedef sa_family_t				iFamily_t;		//< OS 별로 사용되는 Address Family 변수형 재정의.
typedef unsigned long			dword;			//< OS 별로 사용되는 DWORD(자연수) 변수형 재정의.

#define IEOLTYPE				("\n")
#define ITIMEDOUT				ETIMEDOUT
#define ILIB_INFINITE			(-1)
#define ILIBAPI


/******************************************
 * ISOLARIS Type and Macro Definitions
 ******************************************/
#elif defined ISOLARIS
typedef signed long long		int64;			//< 64bit 정수형 Type 재정의.
typedef unsigned long long		uint64;			//< 64bit 자연수 Type 재정의.
typedef socklen_t				iSockLen_t;		//< OS 별로 사용되는 Socket Length 변수형 재정의.
typedef size_t					iSize_t;		//< OS 별로 사용되는 size_t(자연수) 변수형 재정의.
typedef ssize_t					isSize_t;		//< OS 별로 사용되는 ssize_t(정수) 변수형 재정의.
typedef int32					iSem_t;			//< OS 별로 사용되는 Semaphore 변수형 재정의.
typedef int32					iShm_t;			//< OS 별로 사용되는 Shared Memory 변수형 재정의.
typedef key_t 					iKey_t;			//< OS 별로 사용되는 key_t 변수형 재정의.
typedef pthread_t				iThread_t;		//< OS 별로 사용되는 Thread 변수형 재정의.
typedef pthread_cond_t			iEvent_t;		//< OS 별로 사용되는 Event 변수형 재정의.
typedef pthread_mutex_t			iMutex_t;		//< OS 별로 사용되는 Mutex 변수형 재정의.
typedef pthread_rwlock_t		iRwLock_t;		//< OS 별로 사용되는 ReadWrite Lock 변수형 재정의.
typedef pid_t					iPid_t;			//< OS 별로 사용되는 Process Id 변수형 재정의.
typedef sa_family_t				iFamily_t;		//< OS 별로 사용되는 Address Family 변수형 재정의.
typedef unsigned long			dword;			//< OS 별로 사용되는 DWORD(자연수) 변수형 재정의.

#define IEOLTYPE				("\n")
#define ITIMEDOUT				ETIMEDOUT
#define ILIB_INFINITE			(-1)
#define ILIBAPI


#else
#error "Cannot decide the target operating systems. !!!"
#endif



#define NEOLTYPE				IEOLTYPE

#define MAXLEN_PATH				(260)				//< File Path 저장 Buffer 크기.
#define MAXLEN_HEXDUMP			(1024)				//< iHexDump시 화면에 출력할 최대 Binary Data 크기.

#define ILEN_8B					(8)
#define ILEN_16B				(ILEN_8B*2)
#define ILEN_32B				(ILEN_16B*2)
#define ILEN_64B				(ILEN_32B*2)
#define ILEN_128B				(ILEN_64B*2)
#define ILEN_256B				(ILEN_128B*2)
#define ILEN_512B				(ILEN_256B*2)
#define ILEN_1K					(ILEN_512B*2)
#define ILEN_2K					(ILEN_1K*2)
#define ILEN_4K					(ILEN_2K*2)
#define ILEN_8K					(ILEN_4K*2)
#define ILEN_16K				(ILEN_8K*2)
#define ILEN_32K				(ILEN_16K*2)
#define ILEN_64K				(ILEN_32K*2)
#define ILEN_128K				(ILEN_64K*2)
#define ILEN_256K				(ILEN_128K*2)
#define ILEN_512K				(ILEN_256K*2)
#define ILEN_1M					(ILEN_512K*2)
#define ILEN_2M					(ILEN_1M*2)
#define ILEN_4M					(ILEN_2M*2)

#define MAXLEN_LINE				(1024)
#define MAXLEN_LINE4			(4*MAXLEN_LINE)
#define MAXLEN_LINE16			(16*MAXLEN_LINE)
#define MAXLEN_LINE32			(32*MAXLEN_LINE)
#define MAXLEN_LINE64			(64*MAXLEN_LINE)
#define MAXLEN_LINE128			(128*MAXLEN_LINE)
#define MAXLEN_LINE256			(256*MAXLEN_LINE)
#define MAXLEN_LINE512			(512*MAXLEN_LINE)
#define MAXLEN_LINE1M			(MAXLEN_LINE*MAXLEN_LINE)
#define MAXLEN_LINE2M			(2*MAXLEN_LINE*MAXLEN_LINE)
#define MAXLEN_LINE4M			(4*MAXLEN_LINE*MAXLEN_LINE)

#define STRLEN_IPv4				(16)
#define BINLEN_IPv4				(4)
#define STRLEN_IPv6				(64)
#define BINLEN_IPv6				(16)
#define STRLEN_MACADDR			(24)
#define BINLEN_MACADDR			(6)

#define MAXLEN_IPADDR4			(STRLEN_IPv4)
#define MAXLEN_IPADDR6			(STRLEN_IPv6)
#define MAXLEN_IPADDR			(MAXLEN_IPADDR6)
#define MAXLEN_MACADDR			(STRLEN_MACADDR)	//< MAC Address 문자열 저장 길이.
#define MAXLEN_DATE				(16)				//< YYYYMMDD 형식의 문자열 저장 길이.
#define MAXLEN_TIME				(8)					//< hhmmss 형식의 문자열 저장 길이.
#define MAXLEN_DATETIME			(MAXLEN_DATE+MAXLEN_TIME)

#define SIN4ADDR_ANY			("0.0.0.0")			//< IPv4 형식의 LoopBack 주소 문자열.
#define SIN6ADDR_ANY			("::0")				//< IPv6 형식의 LoopBack 주소 문자열.
#define SLOCAL_HOST4			("127.0.0.1")		//< IPv4 형식의 Localhost 주소 문자열.
#define SLOCAL_HOST6			("::1")				//< IPv6 형식의 Localhost 주소 문자열.

#define IMAX(x, y)				((x)>(y)?(x):(y))
#define IMIN(x, y)				((x)<(y)?(x):(y))
#define IMAKEWORD(h, l)			((h)<<16|((l)&0xffff))
#define IHIWORD(v)				((v)>>16)
#define ILOWORD(v)				((v)&0xffff)
#define IMKFOURCC(n)			((((n)&0x000000ff)<<24)|(((n)&0x0000ff00)<<8)|(((n)&0x00ff0000)>>8)|(((n)&0xff000000)>>24))
#define IMKTWOCC(n)				((((n)&0x00ff)<<8)|(((n)&0xff00)>>8))
#define IMK8CC(a,b,c,d,e,f,g,h)	((((h)&0xff)<<56)|(((g)&0xff)<<48)|(((f)&0xff)<<40)|(((e)&0xff)<<32)|(((d)&0xff)<<24)|(((c)&0xff)<<16)|(((b)&0xff)<<8)|((a)&0xff))
#define IMK4CC(a,b,c,d)			((((d)&0xff)<<24)|(((c)&0xff)<<16)|(((b)&0xff)<<8)|((a)&0xff))
#define IMK2CC(a,b)				((((b)&0xff)<<8)|((a)&0xff))

#define IALIGN(n,b)				((((n)+(b)-1)/(b))*(b))
#define IALIGN4(n)				IALIGN(n,4)
#define IALIGN32(n)				IALIGN(n,32)

#define IROUND(x,y)				(((x)+((y)-1))&~((y)-1))
#define IROUND4(x)				(IROUND(x,4))

#define IDELPTR(x)				if(x){delete(x);(x)=NULL;}
#define IDELARR(x)				if(x){delete[](x);(x)=NULL;}
#define ZEROARR(x)				memset((x),0x00,sizeof(x))
#define ZEROCLS(x)				memset(&(x),0x00,sizeof(x))
#define IABS(n)					((n)>0?(n):-(n))

#define ONESEC					(1000)				//< 1초(Milisec 단위)
#define TENSEC					(ONESEC*10)			//< 10초(Milisec 단위)
#define ONEMIN					(ONESEC*60)			//< 1분(Milisec 단위)
#define ONEHOUR					(ONEMIN*60)			//< 1시간(Milisec 단위)
#define ONEDAY					(ONEHOUR*24)		//< 1일(Milisec 단위)
#define ONEWEEK					(ONEDAY*7)			//< 1주일(Milisec 단위)

#define MIN2SEC					(60)				//< 1분(Second 단위)
#define HOUR2SEC				(60*MIN2SEC)		//< 1시간(Second 단위)
#define DAY2SEC					(24*HOUR2SEC)		//< 1일(Second 단위)
#define WEEK2SEC				(7*DAY2SEC)			//< 1주일(Second 단위)

#define ILIB_NULLCHAR			('\0')

#define IMAX_INT8				(127)
#define IMIN_INT8				(-128)
#define IMAX_INT16				(32767)
#define IMIN_INT16				(-32768)
#define IMAX_INT32				(2147483647)
#define IMIN_INT32				(-2147483648)
#define IMAX_INT64				(9223372036854775807L)
#define IMIN_INT64				(-9223372036854775808L)
#define IMAX_UINT8				(255)
#define IMAX_UINT16				(65535)
#define IMAX_UINT32				(4294967295UL)
#define IMAX_UINT64				(18446744073709551615UL)


/******************************************
 * iCore Library Type Definitions
 ******************************************/

typedef void *					NPVOID;				//< void * 형 재정의.
typedef const void *			NPCVOID;			//< const void * 형 재정의.

typedef char *					NPSTR;				//< char * 형 재정의.
typedef const char *			NPCSTR;				//< const char * 형 재정의.
typedef char const *			NPSTRC;				//< char const * 형 재정의.
typedef const char * const		NPCSTRC;			//< const char * const 형 재정의.

typedef uchar *					UPSTR;				///< uchar * 형 재정의.
typedef const uchar *			UPCSTR;				///< const uchar * 형 재정의.
typedef uchar const *			UPSTRC;				///< uchar const * 형 재정의.
typedef const uchar * const		UPCSTRC;			///< const uchar * const 형 재정의.

typedef uchar					iBit8_t;			///< 8bit 저장 변수 형 정의.
typedef uint16					iBit16_t;			///< 16bit 저장 변수 형 정의.
typedef uint32					iBit32_t;			///< 32bit 저장 변수 형 정의.
typedef uint64					iBit64_t;			///< 64bit 저장 변수 형 정의.
typedef uchar					iByte_t;			///< Binary Data 저장 변수 형 정의.
typedef char 					iChar_t;			///< Charater 저장 변수 형 정의.
typedef int32					iFd_t;				///< File이나 Socket Descriptor 형 정의.
typedef iFd_t					iFd;				///< 앞으로 사용 불가.
typedef off_t 					iOff_t;				///< Size와 비슷하지만 Offset 값을 저장
typedef NPVOID					iPos_t;				///< Position 저장 변수 형 정의.
typedef NPVOID					iHandle_t;			///< Handle 저장 변수 형 정의.
typedef int64					iTime_t;			///< 32/64bit OS간 시간 정보 통일을 위한 변수 형 정의.
													///< Second/Milisec/Microsec/Nanosec 등을 저장함.

typedef struct										///< struct timeval 구조체 대체.
{
	iTime_t tSec;									///< seconds since Jan. 1, 1970
	iTime_t uSec;									///< and microseconds
} iTimeVal_t;

typedef struct										///< struct tm 구조체 대체.
{
	uint16	dYear;									///< years since 1900			[0-??]
	uint8	dMon;									///< months since January		[0-11]
	uint8	dDay;									///< day of the month			[1-31]
	uint16	yDay;									///< days since January 1		[0-365]
	uint8	wDay;									///< days since Sunday			[0-6]
	uint8	tHour;									///< hours since midnight		[0-23]
	uint8	tMin;									///< minutes after the hour		[0-59]
	uint8	tSec;									///< seconds after the minute	[0-60]
	int16	nGmt;									///< Minutes east of UTC
	uint16	mSec;									///< milisec after the second	[0-999]
	uint16	uSec;									///< microsec after the milesec	[0-999]
} iTimeInf_t;


#ifdef __cplusplus
	#ifndef NULL
		#define NULL			(0)
	#endif
#else
	#ifndef NULL
		#define NULL			((NPVOID)0)
	#endif
	typedef uchar				bool;
	#define false				(0)
	#define true				(!false)
#endif



#ifdef IUNIX
// for ANSI Color
#define ABLACK					"\e[30m"		// [30m : 글자색:검정
#define ARED					"\e[31m"		// [31m : 글자색:빨강
#define AGREEN					"\e[32m"		// [32m : 글자색:초록
#define AYELOW					"\e[33m"		// [33m : 글자색:노랑
#define ABLUE					"\e[34m"		// [34m : 글자색:파랑
#define APURPLE					"\e[35m"		// [35m : 글자색:마젠트(분홍)
#define ACYAN					"\e[36m"		// [36m : 글자색:시안(청록)
#define AWHITE					"\e[37m"		// [37m : 글자색:백색
#define ADEFLT					"\e[39m"		// [39m : 글자색으로 기본값으로

#define ABBLAK					"\e[40m"		// [40m : 바탕색:흑색
#define ABRED					"\e[41m"		// [41m : 바탕색:적색
#define ABGREN					"\e[42m"		// [42m : 바탕색:녹색
#define ABYELW					"\e[43m"		// [43m : 바탕색:황색
#define ABBLUE					"\e[44m"		// [44m : 바탕색:청색
#define ABPURP					"\e[45m"		// [45m : 바탕색:분홍색
#define ABCYAN					"\e[46m"		// [46m : 바탕색:청록색
#define ABWHIT					"\e[47m"		// [47m : 바탕색:흰색
#define ABDEFT					"\e[49m"		// [49m : 바탕색을 기본값으로

#define ABOLD					"\e[1m"			// [1m  : 굵게(bold) / 밝게
#define AITLIC					"\e[3m"			// [3m  : 이탤릭체(italic)
#define AULINE					"\e[4m"			// [4m  : 밑줄(underline)
#define ABLINK					"\e[5m"
#define AFBLNK					"\e[6m"

#define ANONE					"\e[0m"			// [0m  : 모든 색과 스타일 초기화.

// [7m  : 반전(글자색/배경색을 거꾸로)
// [9m  : 가로줄 치기
// [22m : 굵게(bold) 제거
// [23m : 이탤릭체(italic)제거
// [24m : 밑줄(underline)제거
// [27m : 반전 제거
// [29m : 가로줄 제거

#else
#define ABLACK					""
#define ARED					""
#define AGREEN					""
#define AYELOW					""
#define ABLUE					""
#define APURPLE					""
#define ACYAN					""
#define AWHITE					""
#define ADEFLT					""

#define ABBLAK					""
#define ABRED					""
#define ABGREN					""
#define ABYELW					""
#define ABBLUE					""
#define ABPURP					""
#define ABCYAN					""
#define ABWHIT					""
#define ABDEFT					""

#define ABOLD					""
#define AITLIC					""
#define AULINE					""
#define ABLINK					""
#define AFBLNK					""

#define ANONE					""
#endif
/*
 [0m  : 모든 색과 스타일 초기화
 [1m  : 굵게(bold) / 밝게
 [3m  : 이탤릭체(italic)
 [4m  : 밑줄(underline)
 [7m  : 반전(글자색/배경색을 거꾸로)
 [9m  : 가로줄 치기
 [22m : 굵게(bold) 제거
 [23m : 이탤릭체(italic)제거
 [24m : 밑줄(underline)제거
 [27m : 반전 제거
 [29m : 가로줄 제거
 [30m : 글자색:검정
 [31m : 글자색:빨강
 [32m : 글자색:초록
 [33m : 글자색:노랑
 [34m : 글자색:파랑
 [35m : 글자색:마젠트(분홍)
 [36m : 글자색:시안(청록)
 [37m : 글자색:백색
 [39m : 글자색으로 기본값으로
 [40m : 바탕색:흑색
 [41m : 바탕색:적색
 [42m : 바탕색:녹색
 [43m : 바탕색:황색
 [44m : 바탕색:청색
 [45m : 바탕색:분홍색
 [46m : 바탕색:청록색
 [47m : 바탕색:흰색
 [49m :바탕색을 기본값으로
 */


#if defined IWINDOWS
#define DO_PRAGMA(x) __pragma (x)
#else
#define DO_PRAGMA(x) _Pragma (#x)
#endif

///< Compile Time에 할일에 대한 설명을 표기하도록 함.
#define TODO(x) DO_PRAGMA(message (ABBLUE "TODO - " #x ANONE))	
///< Compile Time에 경고를 표기하도록 함.
#define WARN(x) DO_PRAGMA(message (ABRED "WARN - " #x ANONE))	
///< Compile Time에 노트를 표기하도록 함.
#define NOTE(x) DO_PRAGMA(message (AGREEN "NOTE - " #x ANONE))	


#if defined ISOLARIS
#define PRAGMA_PACK_BEGIN(x)	DO_PRAGMA(pack(x))
#define PRAGMA_PACK_END			DO_PRAGMA(pack())
#else
#define PRAGMA_PACK_BEGIN(x)	DO_PRAGMA(pack(push,x))
#define PRAGMA_PACK_END			DO_PRAGMA(pack(pop))
#endif


#ifdef __GNUC__
#define IDEPRECATED				__attribute__((deprecated))
#elif defined(_MSC_VER)
#define IDEPRECATED				__declspec(deprecated)
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define IDEPRECATED
#endif



///////////////////////////////////////////////////////////////////////////////
/// Coding Style
/// 

#define DISALLOW_COPY_AND_ASSIGN(TypeName)\
private:\
TypeName(const TypeName&) {};\
void operator =(const TypeName&) {}

/**
 * @brief       STL의 map 사용 시 편하게 Type Define을 수행 할 수 있도록 하는 MACRO
 * @param  key  map의 Key Type
 * @param  val  map의 Value Type
 * @param  prfx Type Define 시 Type의 Prefix
 * @note        예제
@code
ITYPEDEF_STDMAP(int,int,iTest);
iTestMap mapInst;
mapInst.insert(iTestSet(1,1));
iTestItr pos = mapInst.begin();
 */
#define ITYPEDEF_STDMAP(key,val,prfx)									\
	typedef std::map<key,val>								prfx##Map;	\
	typedef std::pair<key,val>								prfx##Set;	\
	typedef prfx##Map::iterator								prfx##Itr;	\
	typedef std::pair<prfx##Itr,bool>						prfx##Ret;
#define ITYPEDEF_STDMAPCMP(key,val,cmp,prfx)							\
	typedef std::map<key,val,cmp>							prfx##Map;	\
	typedef std::pair<key,val>								prfx##Set;	\
	typedef prfx##Map::iterator								prfx##Itr;	\
	typedef std::pair<prfx##Itr,bool>						prfx##Ret;

/**
 * @brief       STL의 vector 사용 시 편하게 Type Define을 수행 할 수 있도록 하는 MACRO
 * @param  val  vector의 Value Type
 * @param  prfx Type Define 시 Type의 Prefix
 * @note        예제
@code
ITYPEDEF_STDVEC(int,iTest);
iTestVec vecInst;
vecInst.insert(iTestSet(1,1));
iTestItr pos = vecInst.begin();
 */
#define ITYPEDEF_STDVEC(val,prfx)										\
	typedef std::vector<val>								prfx##Vec;	\
	typedef prfx##Vec::iterator								prfx##Itr;	\

/**
 * @brief       STL의 deque 사용 시 편하게 Type Define을 수행 할 수 있도록 하는 MACRO
 * @param  val  deque의 Value Type
 * @param  prfx Type Define 시 Type의 Prefix
 * @note        예제
@code
ITYPEDEF_STDQUE(int,iTest);
iTestQue queInst;
iTestItr pos = queInst.begin();
 */
#define ITYPEDEF_STDQUE(val,prfx)										\
	typedef std::deque<val>									prfx##Que;	\
	typedef prfx##Que::iterator								prfx##Itr;
/**
 * @brief       STL의 priority_queue 사용 시 편하게 Type Define을 수행 할 수 있도록 하는 MACRO
 * @param  val  priority_queue의 Value Type
 * @param  cmp  priority_queue의 Compare Rule
 * @param  prfx Type Define 시 Type의 Prefix
 * @note        예제
@code
struct iTestCmp
{
	iTestCmp(int nNew) : nVal(nNew) {};
	int nVal;
	bool operator()(const iTestCmp* pLeft, const iTestCmp* pRight)
	{ return pLeft->nVal > pRight->nVal; }
};
ITYPEDEF_STDPQUE(iTestCmp, iTestCmp, iTest);
iTestQue queInst;
queInst.push(iTestCmp(1));
queInst.push(iTestCmp(2));
queInst.top().nVal;
queInst.pop();
 */
#define ITYPEDEF_STDPQUE(val,cmp,prfx)									\
	typedef std::priority_queue<val,std::vector<val>,cmp>	prfx##Que;
/**
 * @brief       STL의 set 사용 시 편하게 Type Define을 수행 할 수 있도록 하는 MACRO
 * @param  val  set의 Value Type
 * @param  prfx Type Define 시 Type의 Prefix
 * @note        예제
@code
ITYPEDEF_STDSET(int,iTest);
iTestSet setInst;
iTestItr pos = setInst.begin();
 */
#define ITYPEDEF_STDSET(val,prfx)											\
	typedef std::set<val>									prfx##Set;		\
	typedef prfx##Set::iterator								prfx##SetPos;


#define IASSERT_SIZE(name, test)		typedef char szAssert##name[(test)*2-1]

#define ISTRUCTDEF_START(name)			struct name##In
#define ISTRUCTDEF_INIT(name)			name##In()
#define ISTRUCTDEF_END(name, minsize)	; typedef struct : public name##In \
										{ char sPad[minsize-sizeof(name##In)]; } name; \
										IASSERT_SIZE(name,sizeof(name)==minsize)

#ifndef __FUNC__
#define __FUNC__						__FUNCTION__
#endif











