
#include "iType.h"
#include "iCnvtVoc.h"
#include "Common.h"
#include "iTest.h"

int main(int argc, char const *argv[])
{
    iCnvtVoc cnvtVoc;
    cnvtVoc.Init("./");
    int32 nRc = CONVERT_VOC_RESULT_SUCCESS;

    nRc = cnvtVoc.ConvertVoc("_input.voc", CONVERT_VOC_AUDIO_TYPE_PCM);
    printf("** Result   : %d\n", nRc);
    printf("** File     : %s\n", cnvtVoc.GetOutFilePath());
    printf("** FileSize : %lld\n\n", cnvtVoc.GetOutFileSize());
    cnvtVoc.Clear();

    nRc = cnvtVoc.ConvertVoc("_input.voc", CONVERT_VOC_AUDIO_TYPE_MP3);
    printf("** Result   : %d\n", nRc);
    printf("** File     : %s\n", cnvtVoc.GetOutFilePath());
    printf("** FileSize : %lld\n\n", cnvtVoc.GetOutFileSize());
    cnvtVoc.Clear();

    // nRc = cnvtVoc.ConvertVoc("_input.voc", CONVERT_VOC_AUDIO_TYPE_PCM);
    // printf("** Result   : %d\n", nRc);
    // printf("** File     : %s\n", cnvtVoc.GetOutFilePath());
    // printf("** FileSize : %lld\n\n", cnvtVoc.GetOutFileSize());
    // cnvtVoc.Clear();

    // nRc = cnvtVoc.ConvertVoc("_input.voc", CONVERT_VOC_AUDIO_TYPE_ALAW);
    // printf("** Result   : %d\n", nRc);
    // printf("** File     : %s\n", cnvtVoc.GetOutFilePath());
    // printf("** FileSize : %lld\n\n", cnvtVoc.GetOutFileSize());
    // cnvtVoc.Clear();

    // nRc = cnvtVoc.ConvertVoc("_input.voc", CONVERT_VOC_AUDIO_TYPE_G723_1);
    // printf("** Result   : %d\n", nRc);
    // printf("** File     : %s\n", cnvtVoc.GetOutFilePath());
    // printf("** FileSize : %lld\n\n", cnvtVoc.GetOutFileSize());
    // cnvtVoc.Clear();

	return 0;
}
































