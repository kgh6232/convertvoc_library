//
//  iVocFile.h
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iVocFile.h
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#pragma once

#include"iFile.h"

#pragma pack(1)
typedef struct {
	uint16		wFormatTag;			/* format type */
	uint16		nChannels;			/* number of channels (i.e. mono, stereo, etc.) */
	uint32		nSamplesPerSec;		/* sample rate */
	uint32		nAvgBytesPerSec;	/* for buffer estimation */
	uint16		nBlockAlign;		/* block size of data */
	uint16		wBitsPerSample;
} WAVEFORMAT;

typedef struct {
	char	szFileFormat[4];
	int32	dwFileSize;
	char	szFileType[4];
	char	szChunkType[4];
	uint32	dwChunkSize;
	WAVEFORMAT stWaveFmt;
	// int16	wParamSize;
	char	szFact[4];
	uint32	dwFactSize;
	uint32	dwFact;
	char	szData[4];
	uint32	dwDataSize;
} WAVE_HEADER_PCM;
#pragma pack()

/**
 * @class    iVocFile
 * @brief    
 */
class iVocFile : public iFile
{
public:
	iVocFile();
	virtual ~iVocFile();

	bool OpenVoc(NPCSTR szVoc);
	void CloseVoc();

	int32 ReadVoc(NPVOID pBuf, int32 nBuf);

protected:
	int32 Xor(NPVOID pSrc, int32 nSrc, NPVOID pDst = NULL);

private:
	uint16 mnIdx;
};









































