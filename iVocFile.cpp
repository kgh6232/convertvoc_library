//
//  iVocFile.cpp
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iVocFile.cpp
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#include "iVocFile.h"

#define	WAVE_FORMAT_PCM				(1)

typedef struct {
	uint16 nYear;
	uint16 nMonth;
	uint16 nDayOfWeek;
	uint16 nDay;
	uint16 nHour;
	uint16 nMin;
	uint16 nSec;
	uint16 nMillisec;
} VELOCE_TIME;	// 만들어진 시간

#pragma pack(1)
typedef struct
{
	uint16			wFormatTag;         /* format type */
	uint16			nChannels;          /* number of channels (i.e. mono, stereo...) */
	uint32			nSamplesPerSec;     /* sample rate */
	uint32			nAvgBytesPerSec;    /* for buffer estimation */
	uint16			nBlockAlign;        /* block size of data */
	uint16			wBitsPerSample;     /* number of bits per sample of mono data */
	uint16			cbSize;             /* the count in bytes of the size of */
										/* extra information (after cbSize) */
} WAVEFORMATEX;

typedef struct MEDIA_FILE_HDR_KEY
{
	uint8			szMediaName		[4];	// Media Name
	uint8			szVersion		[4];	// File Version
	VELOCE_TIME		tStart;
	MEDIA_FILE_HDR_KEY()
	{
		memset(this, 0x00, sizeof(*this));
		memcpy(&szMediaName,	"VLC2",	4);
		memcpy(&szVersion,		"V200",	4);
	}
} MEDIA_FILE_HDR_KEY;

typedef struct MEDIA_FILE_HDR_INFO	// 
{
	uint32			tCall;
	uint8			szRecKey		[24];		// Recording Key
	uint8			szCallID		[100];		// Call ID
	uint8			szCallKind		[4];	// Call Kind '1' : Inbound, 'O' : Outbound
	MEDIA_FILE_HDR_INFO()
	{
		memset(this, 0x00, sizeof(*this));
	}
} MEDIA_FILE_HDR_INFO;

typedef struct MEDIA_FILE_HDR_AGENT
{
	uint32			nLenAgentField;
	uint8			szTenantID		[24];	// Tenant ID
	uint8			szAgentID		[24];	// AGENT ID
	uint8			szGroupID		[24];	// Group ID
	uint8			szAgentDN		[24];	// AGENT DN
	uint8			szAgentIP		[24];	// AGENT IP
	uint32			nRtpPort;
	MEDIA_FILE_HDR_AGENT()
	{
		memset(this, 0x00, sizeof(*this));
		nLenAgentField = sizeof(*this);
	}
} MEDIA_FILE_HDR_AGENT;

typedef struct MEDIA_FILE_HDR_CUST
{
	uint32			nLenCustField;		// Header Size
	uint8			szCustName[24];		// Cust Name
	uint8			szCustTel[24];		// Cust Tel
	uint8			szCustNo[24];		// Cust	No
	uint8			szCustETC[256];		// Cust ETC Info

	MEDIA_FILE_HDR_CUST()
	{
		memset(this, 0x00, sizeof(*this));
		nLenCustField = sizeof(*this);
	}
} MEDIA_FILE_HDR_CUST;

typedef struct MEDIA_PLAY_MARK
{
	uint8			szDesc[32];
	uint32			nIndex;
} MEDIA_PLAY_MARK;

typedef struct MEDIA_FILE_HDR_MEDIA
{
	uint32			nLenMediaData;						// Header Size
	uint32			nMediaType;							// Media type
	uint32			nBlockSize;							// Writing Block Size
	uint32			nTotalDataSize;						// Total Data size(Payload)
	MEDIA_PLAY_MARK	stMark[10];
	MEDIA_FILE_HDR_MEDIA()
	{
		nBlockSize	= 24;
		memset(this, 0x00, sizeof(*this));
		nLenMediaData	= sizeof(*this);
	}
} MEDIA_FILE_HDR_MEDIA;


#define	MEDIA_VOICE_FILLER_SIZE	(14)
typedef struct MEDIA_FILE_MEDIA_VOICE
{
	WAVEFORMATEX	waveFmt;
	uint8			szFiller[MEDIA_VOICE_FILLER_SIZE];
	MEDIA_FILE_MEDIA_VOICE()
	{
		waveFmt.nChannels		= 2;
		waveFmt.nSamplesPerSec	= 8000;
		waveFmt.wBitsPerSample	= 8;
		waveFmt.nAvgBytesPerSec	= waveFmt.nChannels * waveFmt.nSamplesPerSec * waveFmt.wBitsPerSample / 8;
		waveFmt.nBlockAlign		= 1;
		waveFmt.cbSize			= 0;
		waveFmt.wFormatTag		= WAVE_FORMAT_PCM;
		
		memset(szFiller, 0x20, sizeof(szFiller));
	}
} MEDIA_FILE_MEDIA_VOICE;

typedef struct MEDIA_FILE_HEADER_V2
{
	MEDIA_FILE_HDR_KEY		stHdrKey;
	MEDIA_FILE_HDR_INFO		stHdrInfo;
	MEDIA_FILE_HDR_AGENT	stHdrAgent;
	MEDIA_FILE_HDR_CUST		stHdrCust;
	MEDIA_FILE_HDR_MEDIA	stHdrMedia;	
	MEDIA_FILE_MEDIA_VOICE	stHdrMediaFmt;

	MEDIA_FILE_HEADER_V2()
	{
		memset(&stHdrAgent, 0x0, sizeof(stHdrAgent));
		memset(&stHdrCust, 0x0, sizeof(stHdrCust));
	}

	// KeyFormat	MYMMDDHHMMSSxxxx
	//				M + year(1) + MM + DD + HH + MM + SS + DN(4)

	void SetKeyInfo(NPSTR pszRecTime, NPSTR pszDn, NPSTR pszCallID, NPSTR pszCallKind)
	{
		char szYear[5];
		char szRecTime[14];

		memset(szYear, 0x00, sizeof(szYear));
		strncpy(szYear, pszRecTime+2, 2);

		memset(szRecTime, 0x00, sizeof(szRecTime));
		strncpy(szRecTime, pszRecTime+4, 10);

		int32	nYear = atoi(szYear);
		char	chYear;

		if (nYear < 10) 
			chYear = '0' + nYear;
		else
			chYear = 'A' + nYear - 10;

		sprintf( (NPSTR) stHdrInfo.szRecKey,	"M%c%s%s", chYear, szRecTime, pszDn);
		sprintf( (NPSTR) stHdrInfo.szCallID,	"%s", pszCallID);
		sprintf( (NPSTR) stHdrInfo.szCallKind,	"%s", pszCallKind);
	}

	void SetAgentInfo(NPSTR pszTenantID, NPSTR pszAgentID, NPSTR pszGroupID, NPSTR pszAgentDN)
	{
		sprintf( (NPSTR) stHdrAgent.szTenantID,	"%s", pszTenantID);
		sprintf( (NPSTR) stHdrAgent.szAgentID,	"%s", pszAgentID);
		sprintf( (NPSTR) stHdrAgent.szGroupID,	"%s", pszGroupID);
		sprintf( (NPSTR) stHdrAgent.szAgentDN,	"%s", pszAgentDN);
	}

	void SetCustInfo(NPSTR pszCustName, NPSTR pszCustTel, NPSTR pszCustNo, NPSTR pszCustETC)
	{
		sprintf( (NPSTR) stHdrCust.szCustName,	"%s", pszCustName);
		sprintf( (NPSTR) stHdrCust.szCustTel,	"%s", pszCustTel);
		sprintf( (NPSTR) stHdrCust.szCustNo,	"%s", pszCustNo);
		sprintf( (NPSTR) stHdrCust.szCustETC,	"%s", pszCustETC);
	}
} MEDIA_FILE_HEADER_V2;

#pragma pack()


// static const char cHashTable[256] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

static const uint8 ucSeed[256] = {
	0xb8, 0x1e, 0x28, 0x9b, 0x22, 0x6f, 0xa8, 0xc5, 0x28, 0x48,
	0x60, 0x9a, 0x7b, 0x63, 0xc2, 0xcc, 0x2b, 0x8c, 0x7b, 0x9d,
	0x3a, 0x28, 0xa4, 0x47, 0xa3, 0x97, 0x87, 0xf1, 0x38, 0x35,
	0xc5, 0xc2, 0xd1, 0x56, 0xd2, 0xab, 0x79, 0xcc, 0x36, 0xd2,
	0xda, 0xcc, 0x67, 0x81, 0x9a, 0xaa, 0x22, 0x16, 0x8c, 0x93,
	0xa5, 0x25, 0x69, 0x95, 0xa0, 0xd2, 0x96, 0x59, 0xb5, 0x72,
	0x8e, 0x7a, 0xa2, 0x72, 0x9d, 0x49, 0xe5, 0xf2, 0x9b, 0x6d,
	0xb4, 0x61, 0x28, 0x15, 0x9d, 0xa1, 0x12, 0x83, 0x27, 0x5d,
	0xcc, 0x9c, 0xb8, 0x6a, 0x91, 0xd8, 0x2b, 0x61, 0x57, 0x7b,
	0x9b, 0x8d, 0xd4, 0x98, 0xd2, 0x68, 0xc9, 0x6a, 0x4a, 0xdb,
	0xc1, 0x9e, 0xcc, 0x45, 0x04, 0x43, 0x37, 0x20, 0x10, 0x88,
	0x97, 0x5d, 0xa5, 0x48, 0x8e, 0x95, 0xd3, 0xd3, 0xef, 0x12,
	0x1e, 0xb9, 0x04, 0x97, 0x2b, 0x09, 0xc2, 0x9e, 0xcc, 0xb2,
	0x82, 0xa6, 0x42, 0xe5, 0xb7, 0xa1, 0xcc, 0x63, 0x62, 0x4c,
	0xb7, 0xcc, 0xa5, 0x2d, 0x9f, 0x97, 0x37, 0x54, 0x83, 0xb9,
	0x36, 0x7b, 0x7b, 0x82, 0xd3, 0xa3, 0x76, 0xba, 0xa2, 0x8a,
	0x68, 0xcc, 0x33, 0x43, 0x7d, 0xd2, 0x03, 0xcd, 0xa0, 0x9e,
	0xc3, 0x94, 0xa5, 0x89, 0xa1, 0x34, 0x96, 0x0b, 0x3c, 0xb0,
	0x09, 0x3c, 0xe7, 0xc4, 0xa1, 0x9e, 0x9b, 0x4d, 0x0b, 0x96,
	0x75, 0xd2, 0x61, 0xb6, 0x63, 0x1c, 0x63, 0xd2, 0xa7, 0xb3,
	0x67, 0x9d, 0x59, 0x5b, 0x25, 0x82, 0x00, 0x91, 0x3b, 0xc3,
	0xbb, 0x9b, 0x35, 0xb7, 0x08, 0x41, 0x62, 0x27, 0x37, 0x80,
	0x2f, 0x63, 0xd2, 0x11, 0x43, 0x9b, 0xca, 0x3e, 0x5d, 0xf8,
	0x37, 0x60, 0x51, 0x1c, 0x15, 0x4a, 0x48, 0x6f, 0x2b, 0xa8,
	0xa4, 0x23, 0x92, 0x98, 0xa1, 0x67, 0x48, 0x75, 0xb8, 0x5a,
	0x7a, 0xee, 0x9b, 0x8d, 0x18, 0xa0
};

// static const uint32 uiSeed[64] = {
// 	0xb81e289b, 0x226fa8c5, 0x2848609a, 0x7b63c2cc, 
// 	0x2b8c7b9d, 0x3a28a447, 0xa39787f1, 0x3835c5c2, 
// 	0xd156d2ab, 0x79cc36d2,	0xdacc6781, 0x9aaa2216, 
// 	0x8c93a525, 0x6995a0d2, 0x9659b572,	0x8e7aa272, 
// 	0x9d49e5f2, 0x9b6db461, 0x28159da1, 0x1283275d,
// 	0xcc9cb86a, 0x91d82b61, 0x577b9b8d, 0xd498d268, 
// 	0xc96a4adb,	0xc19ecc45, 0x04433720, 0x1088975d, 
// 	0xa5488e95, 0xd3d3ef12,	0x1eb90497, 0x2b09c29e, 
// 	0xccb282a6, 0x42e5b7a1, 0xcc63624c,	0xb7cca52d, 
// 	0x9f973754, 0x83b9367b, 0x7b82d3a3, 0x76baa28a,
// 	0x68cc3343, 0x7dd203cd, 0xa09ec394, 0xa589a134, 
// 	0x960b3cb0,	0x093ce7c4, 0xa19e9b4d, 0x0b9675d2, 
// 	0x61b6631c, 0x63d2a7b3,	0x679d595b, 0x25820091, 
// 	0x3bc3bb9b, 0x35b70841, 0x62273780,	0x2f63d211, 
// 	0x439bca3e, 0x5df83760, 0x511c154a, 0x486f2ba8,
// 	0xa4239298, 0xa1674875, 0xb85a7aee, 0x9b8d18a0
// };

static uint16 CalcIndex(VELOCE_TIME* pTime);



iVocFile::iVocFile()
{
	mnIdx = rand() % 256;
}

iVocFile::~iVocFile()
{
	CloseVoc();
}

#define HDRKEY_START	(24)
#define HDRKEY_LENGTH	(132)

bool iVocFile::OpenVoc(NPCSTR szVoc)
{
	if(OpenFile(szVoc, "rb"))
	{
		MEDIA_FILE_HEADER_V2 stVocHdr;
		if(Read((NPSTR)&stVocHdr, sizeof(stVocHdr))==sizeof(stVocHdr))
		{
			mnIdx = CalcIndex(&stVocHdr.stHdrKey.tStart);
			Xor(&stVocHdr.stHdrInfo, sizeof(stVocHdr.stHdrInfo));
			Xor(&stVocHdr.stHdrAgent, sizeof(stVocHdr.stHdrAgent));
			Xor(&stVocHdr.stHdrCust, sizeof(stVocHdr.stHdrCust));
			Xor(&stVocHdr.stHdrMedia, sizeof(stVocHdr.stHdrMedia));
			Xor(&stVocHdr.stHdrMediaFmt, sizeof(stVocHdr.stHdrMediaFmt));
			return true;
		}
		else
			printf("[ERR] iVocFile::OpenVoc Read VOC header failed");
	}
	else
		printf("[ERR] iVocFile::OpenVoc Open VOC file failed");
	return false;
}

void iVocFile::CloseVoc()
{
	CloseFile();
}

int32 iVocFile::ReadVoc(NPVOID pBuf, int32 nBuf)
{
	iSize_t nRead = Read((NPSTR)pBuf, nBuf);
	Xor(pBuf, nRead);
	return nRead;
}

int32 iVocFile::Xor(NPVOID pSrc, int32 nSrc, NPVOID pDst)
{
	register NPSTR pFrom = (NPSTR)pSrc;
	register NPSTR pTo = (NPSTR)pSrc;
	if (pDst != NULL) pTo = (NPSTR)pDst;
	for (register int32 i=0; i<nSrc; i++)
	{
		if (++mnIdx > 255) mnIdx = 0;
		*pTo = *pFrom ^ ucSeed[mnIdx];
		pFrom++;
		pTo++;
	}
	return mnIdx;
}

























uint16 CalcIndex(VELOCE_TIME* pTime)
{
	uint32 nCheckSum = 0;

	nCheckSum = pTime->nYear * 3;
	nCheckSum += pTime->nMonth * 79;
	nCheckSum += pTime->nDay * 87;
	nCheckSum += pTime->nHour * 97;
	nCheckSum += pTime->nMin * 103;
	nCheckSum += pTime->nSec * 57;
	nCheckSum += pTime->nMillisec * 123;

	nCheckSum = ((nCheckSum >> 16) & 0xffff ) ^ (nCheckSum & 0xffff);
	uint16 nIdx = ((nCheckSum >> 8) & 0xff) ^ (nCheckSum & 0xff);
	nIdx &= 0xff;
	return nIdx;
}






