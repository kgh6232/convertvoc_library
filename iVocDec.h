//
//  iVocDec.h
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iVocDec.h
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#pragma once

#include "iType.h"

extern "C"
{
#if (defined (_WIN32) || defined (_WIN64))
#include <libavformat/avformat.h> 
#include <libavcodec/avcodec.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#else
#include <libavformat/avformat.h> 
#include <libavcodec/avcodec.h>
#include <libswresample/swresample.h>
#include <libswscale/swscale.h>
#include <libavutil/opt.h>
#endif
};

#include <lame/lame.h>

#include "iData.h"

#define DEFLEN_AVCODEC_BUFFER			(ILEN_16K)
#define WAVE_FORMAT_PCM					(0X0001)
#define WAVE_FORMAT_ALAW				(0X0006)
#define WAVE_FORMAT_MULAW				(0X0007)

/**
 * @class    iVocDec
 * @brief    
 */
class iVocDec
{
public:
	iVocDec();
	virtual ~iVocDec();

	bool InitDecoder(int nCodecID, int nChannel, int nSampleRate);
	bool InitDecoderMp3(int nChannel, int nSampleRate);
	void FreeDecoder();

	int32 Decode(const iByte_t* pSrc, int32 nSrc, iByte_t* pDst, int32 nDst, int32 nFmt = WAVE_FORMAT_PCM);

	void DispHelper();
	void FlushDecoder();

	int toMP3(short int* pSrc, int32 nSrc, iByte_t* pDst);
	// bool toMP3(NPCSTR szIn, NPCSTR szOut);
	int64 GetMp3Size() {	return mnMP3Size;	};

	inline AVCodecContext* Ctx() { return mpCtx; };

protected:
	int32 Decode(AVCodecContext* pCtx, AVPacket* pPkt, AVFrame* pFrame, iData& rData, int32 nFmt);

	void Clear();

private:
	AVCodec* mpCodec;
	AVCodecContext* mpCtx;
	AVCodecParserContext* mpParser;
	AVPacket* mpPkt;
	AVFrame* mpFrame;

	/* mp3 */
	lame_t mstLame;

	iByte_t msBuf[DEFLEN_AVCODEC_BUFFER + AV_INPUT_BUFFER_PADDING_SIZE];
	uint64 mnMP3Size;

};





































