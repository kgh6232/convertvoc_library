//
//  iData.cpp
//  
//
//  Created by Tony Ryu on 2020.  .  .
//  Copyright (c) 2020 Tony Ryu. All rights reserved.
//

/**
 * @file     iData.cpp
 * @brief    
 *
 * @author   Tony Ryu(tonyappz@gmail.com)
 * @version  v0.1
 * @see      None
 * @date     2020.  .  .
 */

#include "iData.h"


iData::iData(iSize_t nUnit)
{
	mnUnit = IMAX(nUnit, DEFLEN_ALLOC);
	mnData = mnAloc = 0;
	mvData = NULL;
}

iData::iData(const iData& rData)
{
	mnUnit = DEFLEN_ALLOC;
	mnData = mnAloc = 0;
	mvData = NULL;
	*this = rData;
}

iData::iData(NPCSTR szStr)
{
	mnUnit = DEFLEN_ALLOC;
	mnData = mnAloc = 0;
	mvData = NULL;
	SetString(szStr);
}

iData::iData(const iByte_t* pBytes, iSize_t nBytes)
{
	mnUnit = DEFLEN_ALLOC;
	mnData = mnAloc = 0;
	mvData = NULL;
	SetBytes(pBytes, nBytes);
}

iData::~iData()
{
	Clear();
}

void iData::SetData(const iData& rData)
{
	Clear();
	*this = rData;
}

void iData::SetString(NPCSTR szStr)
{
	Clear();
	if(szStr)
	{
		mnData = strlen(szStr);
		mnAloc = IALIGN(mnData+1, mnUnit);
		mvData = new iByte_t[mnAloc];
		if(mvData)
		{
			memset(mvData, 0x00, mnAloc);
			memcpy(mvData, szStr, mnData);
		}
	}
}

void iData::SetBytes(const iByte_t* pBytes, iSize_t nBytes)
{
	Clear();
	if(pBytes && nBytes>0)
	{
		mnData = nBytes;
		mnAloc = IALIGN(mnData, mnUnit);
		mvData = new iByte_t[mnAloc];
		if(mvData)
		{
			memset(mvData, 0x00, mnAloc);
			memcpy(mvData, pBytes, mnData);
		}
	}
}

void iData::AddString(NPCSTR szAdd)
{
	if(szAdd)
	{
		iSize_t nBytes = strlen(szAdd);
		iSize_t nData = mnData + nBytes;
		if(mnAloc < (nData+1))
		{
			iSize_t nAloc = IALIGN(nData+1, mnUnit);
			iByte_t* vData = new iByte_t[nAloc];
			if(vData)
			{
				memset(vData, 0x00, nAloc);
				memcpy(vData, mvData, mnData);
				IDELARR(mvData);
				mnAloc = nAloc;
				mvData = vData;
			}
		}
		memcpy(mvData+mnData, szAdd, nBytes);
		mnData += nBytes;
	}
}

void iData::AddBytes(const iByte_t* pBytes, iSize_t nBytes)
{
	if(pBytes && nBytes>0)
	{
		iSize_t nData = mnData + nBytes;
		if(mnAloc < nData)
		{
			iSize_t nAloc = IALIGN(nData, mnUnit);
			iByte_t* vData = new iByte_t[nAloc];
			if(vData)
			{
				memset(vData, 0x00, nAloc);
				memcpy(vData, mvData, mnData);
				IDELARR(mvData);
				mnAloc = nAloc;
				mvData = vData;
			}
		}
		memcpy(mvData+mnData, pBytes, nBytes);
		mnData += nBytes;
	}
}

void iData::AddByte(const iByte_t cByte)
{
	iSize_t nData = mnData + sizeof(cByte);
	if(mnAloc < nData)
	{
		iSize_t nAloc = IALIGN(nData, mnUnit);
		iByte_t* vData = new iByte_t[nAloc];
		if(vData)
		{
			memset(vData, 0x00, nAloc);
			memcpy(vData, mvData, mnData);
			IDELARR(mvData);
			mnAloc = nAloc;
			mvData = vData;
		}
	}
	memcpy(mvData+mnData, &cByte, sizeof(cByte));
	mnData += sizeof(cByte);
}

iData& iData::operator=(const iData& rData)
{
	mnUnit = rData.mnUnit;
	SetBytes(rData.Bytes(), rData.Size());
	return *this;
}

iData& iData::operator=(NPCSTR szStr)
{
	SetString(szStr);
	return *this;
}

void iData::Clear()
{
	IDELARR(mvData);
	mnData = mnAloc = 0;
}

































